class Maps{
  private HashMap<PVector, Sphere> spheres;
  private HashMap<String, Sphere> spheresNames;
  private String name;
  private int progress_sum = 0;
  private int nExpected;
  private boolean isLoaded = false;
  private PVector present;
  private ArrayList<PVector> closest;
  private Solid player;
  private PVector toLoad;
  private PVector first;
  
  private PVector empty;
  private float dMax, d2;
  
  public Maps(String name){
    this.name = name;
    spheres = new HashMap<>();
    spheresNames = new HashMap<>();
    closest = new ArrayList<PVector>();
    first = new PVector(0, 0, 0);
    empty = new PVector(0, 0, 0);
  }
  
  public void beginLoading(){
  }
  
  public void loadSpheres(){
  }
  
  public void addSphere(String filename, float x, float y, float z){
    Sphere sphere = new Sphere(filename, x, y, z);
    PVector coordinates = new PVector(x, y, z);
    spheres.put(coordinates, sphere);
    spheresNames.put(filename, sphere);
    progress_sum += 1;
    closest.add(coordinates);
    //spheres.get(coordinates).display();
  }
  
  public void preDisplay(){
    for(PVector keyy : spheres.keySet()){
      spheres.get(keyy).display();
      progress_sum += 1;
    }
  }
  
  public void displayAll(){
    for(PVector keyy : spheres.keySet()){
      spheres.get(keyy).display();
    }
  }
  
  public void displayClosestSphere(){
    first = null;
    first = empty;
    for(PVector v_key : spheres.keySet()) first = v_key;
      dMax = PVector.sub(first, player.getPosition()).mag();
    
    for(PVector v_key : spheres.keySet()){
      d2 = PVector.sub(v_key, player.getPosition()).mag();
      if(d2 < dMax){
        dMax = d2;
        first = v_key;
      }
    }
  // if(!spheres.get(first).isLoaded()) spheres.get(first).load();
    spheres.get(first).display();
  }
  
  public Sphere getClosestSphere(){
    first = null;
    first = empty;
    for(PVector v_key : spheres.keySet()){
      if(first != null) present = first;
      first = v_key;
    }
    dMax = PVector.sub(first, player.getPosition()).mag();
    
    for(PVector v_key : spheres.keySet()){
      float d2 = PVector.sub(v_key, player.getPosition()).mag();
      if(d2 < dMax){
        dMax = d2;
        first = v_key;
      }
    }
    return spheres.get(first);
  }
  
  public void setSolid(Solid solid){
    player = solid;
  }
  
  public void orderSpheres(){
    for(int i=0; i < closest.size() - 1; i++){
      float d1 = PVector.sub(player.getPosition(), closest.get(i + 1)).mag();
      float d2 = PVector.sub(player.getPosition(), closest.get(i)).mag();
      if(d1 < d2){
        PVector temp = closest.get(i);
        closest.set(i, closest.get(i + 1));
        closest.set(i + 1, temp);
      }
    }
    println("///////////////////////smalll");
    for(int i = 0; i<closest.size(); i++){
      if(spheres.get(closest.get(i)).isLoaded()){
        print("loaded  ");
      }else{
        if(spheres.get(closest.get(i)).isLoading()){
          print("loading ");
        }else{
          print("no      ");
        }
      }
      println(PVector.sub(player.getPosition(), closest.get(i)).mag());
    }
    println("biiiiiiiiigggggggggggggg");
    println("");
    for(int i=0; i<5; i++){
      if(!spheres.get(closest.get(i)).isLoaded() && !spheres.get(closest.get(i)).isLoading()){
      //  spheres.get(closest.get(i)).load();
        toLoad = closest.get(i);
        thread("loadInBackground");
      }
    }
    for(int i=5; i<closest.size(); i++) spheres.get(closest.get(i)).unLoad();
  }
  
  public void loadInBackGround(){
    spheres.get(toLoad).load();
  }
  
  public Sphere getSphere(String filename){
    return spheresNames.get(filename);
  }
  
  public float getProgress(){;
    return 100 * (float(progress_sum)/ float(nExpected));
  }
  
  public void setNExpected(int num){
    nExpected = num;
  }
  
  public void loaded(){
    isLoaded = true;
  }
  
  public boolean isLoaded(){
    return isLoaded;
  }
  
}
