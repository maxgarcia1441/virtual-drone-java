class Player{
  private Solid solid; //This will be changed constantly
  private PShape[] bladesA = new PShape[2];
  private PShape[] bladesB = new PShape[2];
  
  private PVector[] bladesA_pos;
  private PVector[] bladesB_pos;
  
  private float[] bladesA_rot;
  private float[] bladesB_rot;
  
  private float d_from_center = 52;
  private float ang = 47;
  private float d_blade = 57.688;
  
  private int i = 0;
  private PVector solid_pos;
  
  private float[] inclination;
  private color color1, color2, color3;
  
  private UDP_server server;
  private String name;
  
  private String displayName;
  private int textSize = 60;
  private Camera cam;
  private boolean tagOn = true;
  
  private boolean flyAlways = false;
  
  private UDP_client client;
  
  private PVector vel, acc;
  
  private String exception;
  private String toSend;
  
  public Player(String name){
    this.name = name;
    displayName = name;
    solid = new Solid(name, loadShape("MainBody.obj"), false, 50, true, 0.9);
    inclination = new float[2];
    
    bladesA_pos = new PVector[2];
    bladesB_pos = new PVector[2];
    bladesA_pos[0] = new PVector(0, 0, 0);
    bladesA_pos[1] = new PVector(0, 0, 0);
    bladesB_pos[0] = new PVector(0, 0, 0);
    bladesB_pos[1] = new PVector(0, 0, 0);
    
    bladesA_rot = new float[2];
    bladesB_rot = new float[2];
    
    update();
  
    bladesA_rot[0] = 0;
    bladesA_rot[1] = 0;
  
    bladesB_rot[0] = 0;
    bladesB_rot[1] = 0;
  
    for(int i=0; i<2; i++){
      PShape bladeA, bladeB;
    
      bladeA = loadShape("propellerA.obj");
      bladeB = loadShape("propellerB.obj");
    
      bladesA[i] = bladeA;
      bladesB[i] = bladeB;
    }
    bladesA[0].rotateX(radians(90));
    bladesA[1].rotateX(radians(90));
    bladesB[0].rotateX(radians(90));
    bladesB[1].rotateX(radians(90));
    
    color1 = color(random(0, 255), random(0, 255), random(0, 255));
    color2 = color(random(0, 255), random(0, 255), random(0, 255));
    color3 = color(random(0, 150), random(0, 150), random(0, 150));
    toSend = "";
  }
  
  private void update(){
    inclination[0] = solid.getA_XZ();
    inclination[1] = solid.getA_XZ_Y();
    
    solid_pos = solid.getPosition();
    
    bladesA_pos[0].x = (cos(radians(ang)) * d_from_center);
    bladesA_pos[0].z = (sin(radians(ang)) * d_from_center);
    bladesA_pos[0].y = 0;
  
    bladesA_pos[1].x = (cos(radians(ang)) * -d_from_center);
    bladesA_pos[1].z = (sin(radians(ang)) * -d_from_center);
    bladesA_pos[1].y = 0;
  
    bladesB_pos[0].x = (cos(radians(ang)) * -d_from_center);
    bladesB_pos[0].z = (sin(radians(ang)) * d_from_center);
    bladesB_pos[0].y = 0;
  
    bladesB_pos[1].x = (cos(radians(ang)) * d_from_center);
    bladesB_pos[1].z = (sin(radians(ang)) * -d_from_center);
    bladesB_pos[1].y = 0;
  }
  
  
  private void takeOff(){
    if(i < 1000) i++;
    bladesA_rot[0] -= i/5;
    bladesA_rot[1] -= i/5;
    bladesB_rot[0] += i/5;
    bladesB_rot[1] += i/5; 
 }
 
  private void land(){
   if(i > 0) i--;
    bladesA_rot[0] -= i/5;
    bladesA_rot[1] -= i/5;
    bladesB_rot[0] += i/5;
    bladesB_rot[1] += i/5;
 }
 
 
 public void display(){
  //thread("takeOff");
  update();
  bladesA_rot[0] = bladesA_rot[0] % 360;
  bladesB_rot[0] = bladesB_rot[0] % 360;
  bladesA_rot[1] = bladesA_rot[1] % 360;
  bladesB_rot[1] = bladesB_rot[1] % 360;
  if(isConnected() || flyAlways){
    takeOff();
  }else{
    land();
  }
  
  if(tagOn){
    pushMatrix();
    textSize(textSize);
    translate(solid.getX(), solid.getY() - 50, solid.getZ());
    rotateY(- radians(cam.getA_XZ() + 90));
    translate(- 70, 0, 0);
    text(displayName, 0, 0, 0);
    popMatrix();
  }
  
  pushMatrix();
  translate(solid_pos.x, solid_pos.y, solid_pos.z);
  rotateY(- radians(inclination[0]));
  rotateZ(- radians(inclination[1]));
  translate(bladesA_pos[1].x, bladesA_pos[1].y, bladesA_pos[1].z);
  rotateY(radians(bladesA_rot[1]));
  translate(-38, 0, 35);
  shape(bladesA[1]);
  bladesA[1].setFill(color1);
  popMatrix();
  
  //stroke(255);
 // line(bladesB_pos[1].x, 0, bladesB_pos[1].z, - bladesB_pos[1].x, 0, bladesB_pos[1].z);
  
  pushMatrix();
  translate(solid_pos.x, solid_pos.y, solid_pos.z);
  rotateY(- radians(inclination[0]));
  rotateZ(- radians(inclination[1]));
  translate(bladesB_pos[1].x, bladesB_pos[1].y - 13, bladesB_pos[1].z);
  rotateY(radians(bladesB_rot[1]));
  translate(-76.4, 0, -70.7);
  bladesB[1].setFill(color2);
  shape(bladesB[1]);
  popMatrix();
  
  pushMatrix();
  translate(solid_pos.x, solid_pos.y, solid_pos.z);
  rotateY(- radians(inclination[0]));
  rotateZ(- radians(inclination[1]));
  translate(bladesA_pos[0].x, bladesA_pos[0].y, bladesA_pos[0].z);
  rotateY(radians(bladesA_rot[0]));
  translate(-38, 0, 35);
  bladesA[0].setFill(color1);
  shape(bladesA[0]);
  popMatrix();
  
  pushMatrix();
  translate(solid_pos.x, solid_pos.y, solid_pos.z);
  rotateY(- radians(inclination[0]));
  rotateZ(- radians(inclination[1]));
  translate(bladesB_pos[0].x, bladesB_pos[0].y - 13, bladesB_pos[0].z);
  rotateY(radians(bladesB_rot[0]));
  translate(-76.4, 0, -70.7);
  bladesB[0].setFill(color2);
  shape(bladesB[0]);
  popMatrix();
  
  solid.display(color3, 0, 0, 90);
  }
  
  public Solid getSolid(){
    return solid;
  }
  
  public void setCamera(Camera cam){
    this.cam = cam; 
  }
  
  public UDP_server getServer(){
    return server;
  }
  
  public void initializeServer(int port, int buffer_size, int timeout){
    server = new UDP_server(name, port, buffer_size, timeout);
    server.initialize();
  }
  
  public boolean isConnected(){
    return server.isConnected();
  }
  
  public void setDisplayName(String newName){
    displayName = newName;
  }
  
  public String getDisplayName(){
    return displayName;
  }
  
  public void tagON(){
    tagOn = true;
  }
  
  public void tagOFF(){
    tagOn = false;
  }
  
  public void setFlight(boolean flight){
    flyAlways = flight;
  }
  
  public String getName(){
    return name;
  }
  
  public UDP_client getUDP_client(){
    return client;
  }
  
  public void initializeClient(){
    client = new UDP_client();
  }
  
  public void sendPosition(){
   // client.send("Position x: " + solid.getX() + " y: " + solid.getY() + " z: " + solid.getZ());
   toSend = toSend.concat("Position x: " + String.format("%.2f", solid.getX()) + " y: " + String.format("%.2f", solid.getY()) + " z: " + String.format("%.2f", solid.getZ()) + "\n");
  }
  
  public void sendVelocity(){
    vel = solid.getVelocity();
    //client.send("Velocity x: " + vel.x + " y: " + vel.y + " z: " + vel.z);
    toSend = toSend.concat("Velocity x: " + String.format("%.2f", vel.x) + " y: " + String.format("%.2f", vel.y) + " z: " + String.format("%.2f", vel.z) + "\n");
  }
  
  public void sendAcceleration(){
    acc = solid.getAcceleration();
   // client.send("Acceleration x: " + acc.x + " y: " + acc.y + " z: " + acc.z);
   toSend = toSend.concat("Acceleration x: " + String.format("%.2f", acc.x) + " y: " + String.format("%.2f", acc.y) + " z: " + String.format("%.2f", acc.z) + "\n");
  }
  public void sendActive(){
    client.send(toSend);
    toSend = displayName + ":\n";
  }
  
  public String getException(){
    return client.getException();
  }
}
