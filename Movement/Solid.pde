class Solid{
  private int w;  //Hitbox           x = width, y = height, z = depth
  private int h;
  private int d;
  
  public PShape shape;  //the shappe itself
  
  public boolean selection;
  
  ////////////// coordinates
  private float x;
  private float y;
  private float z;
  
  private float a_XZ;
  private float a_XZ_Y;
  
  /////////////////////  Physical stuff
  
  private ArrayList<Float[]> loc_x;    // a list of previous coordinates to calculate speed
  private ArrayList<Float[]> loc_y;
  private ArrayList<Float[]> loc_z;
  
  private String name;
  public float mass;
  
  public Float[] speed;
  
  public float rest;
  
  private HashMap<Integer, ArrayList<Float[]>> map = new HashMap<>();
  
  ///////////////// State of the object
  
  private boolean collided;
  private boolean init;
  private boolean movable;
  
  int n_collisions;
  
  /////////////////////   Vectors
  PVector weight;
  
  PVector position;
  PVector velocity;
  PVector acceleration;
  PVector momentum;
  PVector forces;
  
  PVector position_c;
  PVector velocity_c;
  PVector acceleration_c;
  PVector momentum_c;
  PVector forces_c;
  
  float k_e;
  float k_e_c;
  
  ///// Camera stuff
  private PVector obj_position;
  private float d_this_obj;
  
  private PVector dif;
  
  private PVector initial_position;
  private PVector initial_orientation;
  
  public Solid(String name, PShape shape, boolean selection, float mass, boolean movable, float rest){
    this.name = name;
    this.mass = mass;
    this.shape = shape;
    this.movable = movable;
    this.rest = rest;
    
    position = new PVector(0, 0, 0);
    velocity = new PVector(0, 0, 0);
    acceleration = new PVector(0, 0, 0);
    momentum = new PVector(0, 0, 0);
    forces = new PVector(0, 0, 0);
    
    weight = new PVector(0, 0, 0);
    n_collisions = 0;
    
    k_e = 0;
    k_e_c = 0;
    
    // Camera stuff
    obj_position = new PVector(0, 0, 0);
    d_this_obj = 200;
    
    ArrayList<Integer> x_values = new ArrayList<Integer>();
    ArrayList<Integer> y_values = new ArrayList<Integer>();
    ArrayList<Integer> z_values = new ArrayList<Integer>();
   // print(shape.getVertexCount());
    if(selection){
      for(int i = 0; i < shape.getVertexCount(); i++){
        PVector v = shape.getVertex(i);
        x_values.add(int(v.x));
        y_values.add(int(v.y));
        z_values.add(int(v.z));
      }
    }else{
      for(int i = 0; i < shape.getChildCount(); i++){
        PShape temp_shape = shape.getChild(i);
        PVector temp_vec = temp_shape.getVertex(0);
        float[] temp_vert = temp_vec.array();
        x_values.add(int(temp_vert[0]));
        y_values.add(int(temp_vert[2]));
        z_values.add(int(temp_vert[1]));
    }
    }
    w = getMax(x_values) - getMin(x_values);
    h = getMax(y_values) - getMin(y_values);
    d = getMax(z_values) - getMin(z_values);
    
    loc_x = new ArrayList<Float[]>();
    loc_y = new ArrayList<Float[]>();
    loc_z = new ArrayList<Float[]>();
    
    speed = new Float[3];
    
    init = false;
    
    a_XZ = 0;
     a_XZ_Y = 0;
     
    initial_position = position;
    initial_orientation = new PVector(a_XZ, a_XZ_Y);
  }
  
  ////////////////////// Physics
  
  public void update(){
    
    /////vector part
    a_XZ = a_XZ % 360;
    a_XZ_Y = a_XZ_Y % 360;
    velocity.mult(.96);
    
    position.x = x;
    position.y = y;
    position.z = z;
    
    forces.add(weight);
    acceleration = PVector.div(forces, mass);
    velocity.add(acceleration);
    position.add(velocity);
    //acceleration.mult(0);
    forces.mult(0);
    
    x = position.x;
    y = position.y;
    z = position.z;
    
    momentum.x = velocity.x * mass;
    momentum.y = velocity.y * mass;
    momentum.z = velocity.z * mass;
    
    position_c = position;
    velocity_c = velocity;
    momentum_c = momentum;
    
    k_e = (mass * sq(velocity.mag())) / 2;
    
    k_e_c = k_e;
    /////before vectors, needs to be updated
    //////////////x
    Float[] t_posx = new Float[2];
      t_posx[0] = float(millis());
      t_posx[1] = x;
      
    if(loc_x.size() < 10){
      loc_x.add(t_posx);
    }else{
      for(int n = 0; n <= loc_x.size() - 2; n++){
        loc_x.set(n, loc_x.get(n + 1));
      }
      loc_x.set(loc_x.size() - 1, t_posx);
      
      velocity.x = loc_x.get(loc_x.size() - 1)[1] - loc_x.get(loc_x.size() - 2)[1];
    }
    ////////y
    Float[] t_posy = new Float[2];
      t_posy[0] = float(millis());
      t_posy[1] = y;
    if(loc_y.size() < 10){
      loc_y.add(t_posy);
    }else{
      for(int n = 0; n <= loc_y.size() - 2; n++){
        loc_y.set(n, loc_y.get(n + 1));
      }
      loc_y.set(loc_y.size() - 1, t_posy);
      
      velocity.y = loc_y.get(loc_y.size() - 1)[1] - loc_y.get(loc_y.size() - 2)[1];
    }
    /////// z
    Float[] t_posz = new Float[2];
      t_posz[0] = float(millis());
      t_posz[1] = z;
    if(loc_z.size() < 10){
      loc_z.add(t_posz);
    }else{
      for(int n = 0; n <= loc_z.size() - 2; n++){
        loc_z.set(n, loc_z.get(n + 1));
      }
      loc_z.set(loc_z.size() - 1, t_posz);
      
      velocity.z = loc_z.get(loc_z.size() - 1)[1] - loc_z.get(loc_z.size() - 2)[1];
    }
    map.put(0, loc_x);
    map.put(1, loc_y);
    map.put(2, loc_z);
    avg_speed();
    
    rotateObj(0, 0);
  }
  
  public void display(color clr, color stroke, float ca_XZ, float ca_XZ_Y){
    pushMatrix();
    shape.setFill(clr);
    shape.setStroke(stroke);
    translate(x, y, z);
    rotateY(radians(-a_XZ - ca_XZ));
    rotateZ(radians(-a_XZ_Y + 90 - ca_XZ_Y));
    shape(shape);
    popMatrix();
  }
  
  public boolean collided(){
    return collided;
  }
  
  public void setCollided(boolean col){
    collided = col;
  }
  
  public void avg_speed(){
    for(int i=0; i<map.size(); i++){
      if(map.get(i).size() >= 2){
        float dd = 0;
        float dt = 0;
        float sum_d = 0;
        float sum_t = 0;
        for(int n = 0; n <= map.get(i).size() - 2; n++){
          dd = map.get(i).get(n)[1] - map.get(i).get(n + 1)[1];
          dt = map.get(i).get(n)[0] - map.get(i).get(n + 1)[0];
          sum_d += dd;
          sum_t += dt;
        }
        float v = sum_d/sum_t;
        speed[i] = v;
        sum_d = 0;
        sum_t = 0;
        
      }else{
        float v = 0;
        speed[i] = v;
      }
    }
  }
  
  public void collision(Solid solid){
    PVector normal = PVector.sub(position_c, solid.position_c);
    normal.normalize();
    PVector m_relative = PVector.sub(momentum_c,solid.momentum_c);
    float m_dot = m_relative.dot(normal);
    PVector m_normal = PVector.mult(normal, m_dot);
    momentum =  PVector.sub(momentum_c, m_normal);
    velocity = PVector.div(momentum, mass);
    
    solid.momentum =  PVector.add(solid.momentum_c, m_normal);
    solid.velocity = PVector.div(solid.momentum, solid.mass);
    
    k_e = (mass * sq(velocity.mag())) / 2;
    
    push(PVector.mult(forces, -1));
    solid.push(forces);
  }
  
  public void push(PVector force){
    /*PVector f = PVector.div(force, mass);
    acceleration.add(f);*/
    forces.add(force);
  }
  
  public void setWeight(PVector weight){
    this.weight = weight;
  }
  //////////////// Simple Movements
  
  public void move(float dx, float dy, float dz){
    x += dx;
    y += dy;
    z += dz;
  }
  
  public void moveTo(float x, float y, float z){
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  public void moveFirst(float x, float y, float z){
    if(!init){
      this.x = x;
      this.y = y;
      this.z = z;
    }
  }
  
  public void rotate(float da_XZ, float da_XZ_Y) {
    a_XZ += da_XZ;
    a_XZ_Y += da_XZ_Y;
  }
  
  public void rotateTo(float a_XZ, float a_XZ_Y){
    this.a_XZ = a_XZ;
    this.a_XZ_Y = a_XZ_Y;
  }
  
  public boolean movable(){
    return movable;
  }
  
  public PVector getObj_position(){
    return obj_position;
  }
  
  public void rotateObj(float da_XZ, float da_XZ_Y){   //only rotate xz for now
  //r = sqrt(sq(o_x - cam_x) + sq(o_y - cam_y) + sq(o_z - cam_z));   //distance

    a_XZ += da_XZ;   //rotates the axes
    a_XZ_Y += da_XZ_Y;
  
    obj_position.y = d_this_obj * cos(radians(a_XZ_Y));
    obj_position.z = d_this_obj * sin(radians(a_XZ_Y)) * sin(radians(a_XZ));
    obj_position.x = d_this_obj * sin(radians(a_XZ_Y)) * cos(radians(a_XZ));
    
    dif = obj_position;
    obj_position.add(position);
  //  println("Calculation: "+ sin(radians(abs(a_XZ))) + ", r= " + r + "r_XZ= " + r_XZ);
  }
  
  public PVector getDirection(){
    return dif;
  }
  
  public void resetOr(){
    a_XZ -= (a_XZ - initial_orientation.x) / 100;
    a_XZ_Y -= (a_XZ_Y - initial_orientation.y) / 100;
  }
  
  ///////////////////

  
  public int getMax(ArrayList<Integer> values){
    int maxValue = values.get(0);
    for(int i=1; i < values.size(); i++){
      if(values.get(i) > maxValue){
        maxValue = values.get(i);
      }
    }
    return maxValue;
  }
  
  public int getMin(ArrayList<Integer> values){
    int minValue = values.get(0);
    for(int i=1; i < values.size(); i++){
      if(values.get(i) < minValue){
        minValue = values.get(i);
      }
    }
    return minValue;
  }
  
  public String getName(){
    return name;
  }
  
  //////////////// Dimensions
  
  public int getWidth() {
    return w;
  }
  
  public int getHeight() {
    return h;
  }
  
  public int getDepth() {
    return d;
  }
  
  ///////////////////// Position and Orientation
  
  /*public void rotateA(float rad_xz, float rad_xz_y){
    shape.rotateY(rad_xz - radians(a_XZ));
    shape.rotateZ(rad_xz_y - radians(a_XZ_Y));
  }*/
  
  public PShape getShape(){
    return shape;
  }
  
  public PVector getPosition(){
    return position;
  }
  
  public void setPosition(PVector position){
    this.position = position;
  }
  
  public PVector getVelocity(){
    return velocity;
  }
  
  public void setVelocity(PVector velocity){
    this.velocity = velocity;
  }
  
  public PVector getAcceleration(){
    return acceleration;
  }
  
  public void setAcceleration(PVector acceleration){
    this.acceleration = acceleration;
  }
  
  public PVector getObjPosition(){
    return obj_position;
  }
  
  public void setObjPosition(float x, float y, float z){
    PVector pos = new PVector(x, y, z);
    obj_position = pos;
  }
  
  public float getX(){
    return x;
  }
  
  public float getY(){
    return y;
  }
  
  public float getZ(){
    return z;
  }
  
  public float getA_XZ(){
    return a_XZ;
  }
  
  public float getA_XZ_Y(){
    return a_XZ_Y;
  }
  
  public ArrayList<Float[]> pastX() {
    return loc_x;
  }
  
  public ArrayList<Float[]> pastY() {
    return loc_y;
  }
  
  public ArrayList<Float[]> pastZ() {
    return loc_z;
  }
  
  
}
