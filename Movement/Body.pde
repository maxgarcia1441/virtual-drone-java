class Body{   ///////////////////////////////// Essentially useless now
  private float x;
  private float y;
  private float z;
  
  private float a_XZ;
  private float a_XZ_Y;
  
  private float dx;
  private float dy;
  private float dz;
  
  //later add facing to
  
  public Body(){
    dx = 0;
    dy = 0;
    dz = 0;
  }
  
  public void move(float dx, float dy, float dz){
    x += dx;
    y += dy;
    z += dz;
  }

  public void rotate(float da_XZ, float da_XZ_Y) {
    a_XZ += da_XZ;
    a_XZ_Y += da_XZ_Y;
  }
  
  public float getX(){
    return x;
  }
  
  public float getY(){
    return y;
  }
  
  public float getZ(){
    return z;
  }
  
  public float getA_XZ(){
    return a_XZ;
  }
  
  public float getA_XZ_Y(){
    return a_XZ_Y;
  }

}
