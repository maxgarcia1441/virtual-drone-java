class Controls{
  private PVector keys_vector;
  private boolean f, b, r, l, u, d;
  private PVector mouse_Avector;
  private PVector keys_Avector;
  private PVector camera_movement;
  private HashMap<Integer, Boolean> keysPressed;
  private Integer onlyNumber;
  
  private PVector j, center, camera, dir, front, back, left, right, up, down;
  private float x, y;
  private Integer a;
  
  public Controls(){
    keys_vector = new PVector();
    mouse_Avector = new PVector();
    camera_movement = new PVector();
    keys_Avector = new PVector();
    keysPressed = new HashMap<>();
    j = new PVector(0, 0, 0);
  }
  
  public void setKeys(boolean f, boolean b, boolean r, boolean l, boolean u, boolean d){
    this.f = f;
    this.b = b;
    this.r = r;
    this.l = l;
    this.u = u;
    this.d = d;
  }
  
  public void otherKeys(Integer keyy){
    if(!keysPressed.containsKey(keyy)){
      keysPressed.put(keyy, true);
    }else{
      keysPressed.replace(keyy, !keysPressed.get(keyy));
    }
  }
  
  public boolean isPressed(Integer keyy){
    if(keysPressed.containsKey(keyy)){
      return keysPressed.get(keyy);
    }else{
      return false;
    }
  }
  
  public PVector toForce() {
    keys_vector.mult(0);
    if (this.f) keys_vector.z += -10;
    if (this.b) keys_vector.z += 10;
    if (this.r) keys_vector.x += 10;
    if (this.l) keys_vector.x += -10;
    if (this.u) keys_vector.y += -300;
    if (this.d) keys_vector.y += 300;
    return keys_vector;
  }
  
  public PVector moveUseCam(Camera cam){
    camera_movement.mult(0);
    
    j = null;
    j = new PVector(0, -1, 0);
    center = cam.getObj_position();
    camera = cam.getCam_position();
    dir = PVector.sub(center, camera);
    dir.y = 0;
    dir.normalize();
    front = dir;
    back = PVector.mult(front, -1);
    left = dir.cross(j).normalize();
    right = PVector.mult(left, -1);
    up = j;
    down = PVector.mult(j, -1);
    
    if (this.f) camera_movement.add(front);
    if (this.b) camera_movement.add(back);
    if (this.r) camera_movement.add(right);
    if (this.l) camera_movement.add(left);
    if (this.u) camera_movement.add(up);
    if (this.d) camera_movement.add(down);
    
    camera_movement.mult(10);
    //println(camera_movement);
    return camera_movement;
  }
  
  public PVector moveCam(Camera cam){
    camera_movement.mult(0);
    
    j = null;
    j = new PVector(0, -1, 0);
    center = cam.getObj_position();
    camera = cam.getCam_position();
    dir = PVector.sub(center, camera);
    dir.normalize();
    front = dir;
    back = PVector.mult(front, -1);
    left = dir.cross(j).normalize();
    right = PVector.mult(left, -1);
    up = j;
    down = PVector.mult(j, -1);
    
    if (this.f) camera_movement.add(front);
    if (this.b) camera_movement.add(back);
    if (this.r) camera_movement.add(right);
    if (this.l) camera_movement.add(left);
    if (this.u) camera_movement.add(up);
    if (this.d) camera_movement.add(down);
    
    camera_movement.mult(4);
    //println(camera_movement);
    return camera_movement;
  }
  
  public PVector mouse_to_angle(int screen_x, int screen_y){
    x = screen_x - width/2;
    y = height/2 - screen_y;
    if((x > -50 && x < 50) && (y > -50 && y < 50)){
      x = 0;
      y = 0;
    }
    x = x / (width/2);
    y = y / (height/2);
    mouse_Avector.x = x;
    mouse_Avector.y = y;
    return mouse_Avector.mult(2);
  }
  
  public PVector keys_to_angle(){
    keys_Avector.mult(0);
    if(isPressed(37)) keys_Avector.x = - 1;
    if(isPressed(39)) keys_Avector.x = 1;
    keys_Avector.y = 0;
    return keys_Avector;
  }
  
  public void otherKeys(char character){
    a = Character.getNumericValue(character);
    //if(a < 0 || a > 9) a = null;
    onlyNumber = a;
  }
  
  public Integer getNumber(){
    a = onlyNumber;
    if(a < 0 || a > 9){
      return null;
    }else{
      return a;
    }
  }
  
}
