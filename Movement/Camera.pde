class Camera {
  private PVector cam_position;
  
  private float r;
  private float r_XZ;
  int x;
  
  private float a_XZ;
  private float a_XZ_Y;
  
  private PVector obj_position;
  
  private float da_XZ;
  private float da_XZ_Y;
  
  private float d_cam_sld;
  private float d_cam_obj;
  
  private PVector dif;
  private PVector dif_sld_cam;
  
  private boolean follow;
  private Solid solid;
  private PVector copy;
  
  private float[][] pastAng;
  
  //later add facing to
  public Camera() {
    cam_position = new PVector(-464.05096, -157.39027, 51.44919);
    a_XZ = 0;
    a_XZ_Y = 0;
    obj_position = new PVector(418.56805, 18.564682, 46.605118);
    dif = new PVector(0, 0, 0);
    da_XZ = 0;
    da_XZ_Y = 0;
    d_cam_sld = 300;
    d_cam_obj = 900;
    follow = false;
    pastAng = new float[2][20];
    for(int i=0; i<20; i++){
      pastAng[0][i] = 0;
      pastAng[1][i] = 0;
    }
  }
  
  public void moveCTo(PVector position){
    cam_position = position;
  }
  
  public void zoom(float d_zoom){
    d_cam_obj += 100 * d_zoom;
  }
  
  public void moveC(float dx, float dy, float dz){
    cam_position.x += dx;
    cam_position.y += dy;
    cam_position.z += dz;
  }
  
  public void rotateO(float da_XZ, float da_XZ_Y){   //only rotate xz for now
  //r = sqrt(sq(o_x - cam_x) + sq(o_y - cam_y) + sq(o_z - cam_z));   //distance

    a_XZ += da_XZ;   //rotates the axes
    a_XZ_Y += da_XZ_Y;
  
    obj_position.y = d_cam_obj * cos(radians(a_XZ_Y));
    obj_position.z = d_cam_obj * sin(radians(a_XZ_Y)) * sin(radians(a_XZ));
    obj_position.x = d_cam_obj * sin(radians(a_XZ_Y)) * cos(radians(a_XZ));
    
    dif = obj_position.copy();
    obj_position.add(cam_position);
    for(int n = 0; n<19; n++){
      pastAng[0][n] = pastAng[0][n + 1];
      pastAng[1][n] = pastAng[1][n + 1];
    }
    pastAng[0][19] = a_XZ;
    pastAng[1][19] = a_XZ_Y;
    
    a_XZ = a_XZ % 360;
    a_XZ_Y = a_XZ_Y % 360;
  //  println("Calculation: "+ sin(radians(abs(a_XZ))) + ", r= " + r + "r_XZ= " + r_XZ);
  }
  
  public void follow(Solid solid){      //basically third person view
    follow = true;
    this.solid = solid;
      dif_sld_cam = PVector.sub(solid.getPosition(), obj_position);
      copy = dif_sld_cam.copy();
      copy.y -= 300;
      copy.normalize();
      cam_position.mult(0);
      cam_position = PVector.add(solid.position, PVector.mult(copy, d_cam_sld));
  }
  
  public PVector threeAngles(){
    PVector angles = new PVector(0, 0, 0);
    angles.x = degrees(atan(dif.z / dif.y));
    angles.y = degrees(atan(dif.z / dif.x));
    angles.z = degrees(atan(dif.x / dif.y));
    if(dif.z >= 0 && dif.y < 0) angles.x += 180;
    if(dif.z < 0 && dif.y < 0) angles.x += 180;
    if(dif.z < 0 && dif.y > 0) angles.x += 360;
    
    if(dif.z >= 0 && dif.x < 0) angles.y += 180;
    if(dif.z < 0 && dif.x < 0) angles.y += 180;
    if(dif.z < 0 && dif.x > 0) angles.y += 360;
    
    if(dif.x >= 0 && dif.y < 0) angles.z += 180;
    if(dif.x < 0 && dif.y < 0) angles.z += 180;
    if(dif.x < 0 && dif.y > 0) angles.z += 360;
    
    angles.x = angles.x % 360;
    angles.y = angles.y % 360;
    angles.z = angles.z % 360;
    return angles;
  }
  
  public float getPastXZ(){
    return pastAng[0][0];
  }
  
  public float getPastXZ_Y(){
    return pastAng[1][0];
  }
  
  public PVector getCam_position(){
    return cam_position;
  }
  
  public PVector getObj_position(){
    return obj_position;
  }
  
  public float getO_x(){
    return obj_position.x;
  }
  
  public float getO_y(){
    return obj_position.y;
  }
  
  public float getO_z(){
    return obj_position.z;
  }
  
  public float getA_XZ() {
    return a_XZ;
  }
  
  public float getA_XZ_Y() {
    return a_XZ_Y;
  }
  
  public float getX(){
    return cam_position.x;
  }
  
  public float getY(){
    return cam_position.y;
  }
  
  public float getZ(){
    return cam_position.z;
  }
  
}
