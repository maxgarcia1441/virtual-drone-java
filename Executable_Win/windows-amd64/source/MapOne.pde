class MapOne extends Maps{
  
  public MapOne(){
    super("MapOne");
   /* addSphere("360_0105_Stitch_XHC-min.JPG", 500, -200, 0);
  //  getSphere("360_0105_Stitch_XHC-min.JPG").rotate(
    addSphere("360_0106_Stitch_XHC-min.JPG", 250, -200, 0);
    addSphere("360_0108_Stitch_XHC-min.JPG", 0, -200, 0);
    addSphere("360_0109_Stitch_XHC-min.JPG", -250, -200, 0);
    addSphere("360_0110_Stitch_XHC-min.JPG", -500, -200, 0);
    addSphere("360_0111_Stitch_XHC-min.JPG", -750, -200, 0);
    addSphere("360_0113_Stitch_XHC-min.JPG", -1000, -200, 0);
    addSphere("360_0114_Stitch_XHC-min.JPG", -1250, -200, 0);
    addSphere("360_0115_Stitch_XHC-min.JPG", -1500, -200, 0);
   //w addSphere("360_0116_Stitch_XHC-min.JPG", -750, -200, 0);*/
  }
  
  @Override
  public void beginLoading(){
    thread("loadSpheres");
  }
  
  @Override
  public void loadSpheres(){
    addSphere("360_0105_Stitch_XHC-min.JPG", 800, 0, 0);
    getSphere("360_0105_Stitch_XHC-min.JPG").rotate(-5, 0);
    addSphere("360_0106_Stitch_XHC-min.JPG", 400, 0, 0);
    addSphere("360_0108_Stitch_XHC-min.JPG", 0, 0, 0);
    getSphere("360_0108_Stitch_XHC-min.JPG").rotate(-8, 0);
    addSphere("360_0109_Stitch_XHC-min.JPG", -400, 0, 0);
    getSphere("360_0109_Stitch_XHC-min.JPG").rotate(0, 0);
    addSphere("360_0110_Stitch_XHC-min.JPG", -800, 0, 0);
    getSphere("360_0110_Stitch_XHC-min.JPG").rotate(-7, 0);
    addSphere("360_0111_Stitch_XHC-min.JPG", -1200, 0, 0);
    getSphere("360_0111_Stitch_XHC-min.JPG").rotate(-8, 0);
    addSphere("360_0113_Stitch_XHC-min.JPG", -1600, 0, 0);
    getSphere("360_0111_Stitch_XHC-min.JPG").rotate(-7, 0);
    addSphere("360_0114_Stitch_XHC-min.JPG", -2000, 0, 0);
    addSphere("360_0115_Stitch_XHC-min.JPG", -2400, 0, 0);
    addSphere("360_0116_Stitch_XHC-min.JPG", -2100, 0, -400);
    addSphere("360_0117_Stitch_XHC-min.JPG", -2800, 0, 0);
    addSphere("360_0118_Stitch_XHC-min.JPG", -3200, 0, 0);
    addSphere("360_0119_Stitch_XHC-min.JPG", -3200, 0, -400);
    addSphere("360_0120_Stitch_XHC-min.JPG", -2800, 0, -400);
    addSphere("360_0121_Stitch_XHC-min.JPG", -2800, 0, -800);
    addSphere("360_0122_Stitch_XHC-min.JPG", -3600, 0, 40);
   
    addSphere("360_0123_Stitch_XHC-min.JPG", -3600, 0, -310);
    addSphere("360_0124_Stitch_XHC-min.JPG", -3600, 0, -620);
    addSphere("360_0125_Stitch_XHC-min.JPG", -3600, 0, -820);
    addSphere("360_0126_Stitch_XHC-min.JPG", -4000, 0, 210);
    
    addSphere("360_0127_Stitch_XHC-min.JPG", -4000, 0, -280);
    addSphere("360_0128_Stitch_XHC-min.JPG", -4000, 0, -790);
    addSphere("360_0129_Stitch_XHC-min.JPG", -4000, 0, -1100);
    
    addSphere("360_0130_Stitch_XHC-min.JPG", -4000, 0, -1500);
    addSphere("360_0131_Stitch_XHC-min.JPG", -4000, 0, -1900);
    addSphere("360_0132_Stitch_XHC-min.JPG", -4000, 0, -2300);
    addSphere("360_0133_Stitch_XHC-min.JPG", -4000, 0, -2700);
    addSphere("360_0134_Stitch_XHC-min.JPG", -4000, 0, -3100);
    addSphere("360_0135_Stitch_XHC-min.JPG", -4000, 0, -3500);
    addSphere("360_0136_Stitch_XHC-min.JPG", -4000, 0, -3900);
    addSphere("360_0137_Stitch_XHC-min.JPG", -4000, 0, -4300);
    
    addSphere("360_0138_Stitch_XHC-min.JPG", -4000, 0, 600);
    addSphere("360_0139_Stitch_XHC-min.JPG", -4000, 0, 1100);
    addSphere("360_0140_Stitch_XHC-min.JPG", -4000, 0, 1500);
    addSphere("360_0141_Stitch_XHC-min.JPG", -4000, 0, 1900);
    addSphere("360_0142_Stitch_XHC-min.JPG", -4000, 0, 2300);
    
    addSphere("360_0143_Stitch_XHC-min.JPG", -3400, 0, -2300);
    addSphere("360_0144_Stitch_XHC-min.JPG", -3000, 0, -2300);
    addSphere("360_0145_Stitch_XHC-min.JPG", -2600, 0, -2300);
    addSphere("360_0146_Stitch_XHC-min.JPG", -2200, 0, -2300);
    addSphere("360_0147_Stitch_XHC-min.JPG", -1800, 0, -2360);
    addSphere("360_0148_Stitch_XHC-min.JPG", -1400, 0, -2360);
    addSphere("360_0149_Stitch_XHC-min.JPG", -1000, 0, -2360);
    addSphere("360_0150_Stitch_XHC-min.JPG", -700, 0, -2360);
    addSphere("360_0151_Stitch_XHC-min.JPG", -400, 0, -2360);
    addSphere("360_0152_Stitch_XHC-min.JPG", -100, 0, -2360);
    addSphere("360_0153_Stitch_XHC-min.JPG", 300, 0, -2360);
    addSphere("360_0154_Stitch_XHC-min.JPG", 700, 0, -2360);
    addSphere("360_0155_Stitch_XHC-min.JPG", 900, 0, -2360);
   // preDisplay();
    loaded();
  }
  
}
