class Widgets{
  private HashMap<String, float[]> buttons;
  private HashMap<String, Boolean> buttonsPressed;
  private HashMap<String, PShape> shapes;
  private HashMap<String, Integer> colors;
  private HashMap<String, Integer> alpha;
  private HashMap<String, String> menus;
  private HashMap<String, String> text;
  private String activeMenu;
  private int size;
  private String[] previous;
  ////////////////// I know individual buttons should have been a class, but that just didn't happen...
  // Everything is a button (they can be pressed after all
  
  public Widgets(){ 
    buttons = new HashMap<>();
    buttonsPressed = new HashMap<>();
    shapes = new HashMap<>();
    colors = new HashMap<>();
    alpha = new HashMap<>();
    menus = new HashMap<>();
    text = new HashMap<>();
    activeMenu = "";
    previous = new String[2];
  }
  
  public void addButton(String menu, String name, String filename, float x, float y, float z, float w, float h){
    PImage img = loadImage(filename);
    float[] b = {x, y, z, w, h};
    buttons.put(name, b);
    PShape m1 = createShape();
    m1.beginShape();
    m1.vertex(0, h/2, 0, 0, img.height);
    m1.vertex(0, 0, 0, 0, 0);
    m1.vertex(w/2, 0, 0, img.width, 0);
    m1.vertex(w/2, h/2, 0, img.width, img.height);
    m1.vertex(0, h/2, 0, 0, img.height);
    m1.endShape();
    m1.setTexture(img);
    shapes.put(name, m1);
    menus.put(name, menu);
  }
  
  public void addButton(String menu, String name, color clr, int alpha, float x, float y, float z, float w, float h){
    float[] b = {x, y, z, w, h};
    buttons.put(name, b);
    fill(clr, alpha);
    noStroke();
    PShape m1 = createShape();
    m1.beginShape();
    m1.vertex(0, h/2, 0);
    m1.vertex(0, 0, 0);
    m1.vertex(w/2, 0, 0);
    m1.vertex(w/2, h/2, 0);
    m1.vertex(0, h/2, 0);
    m1.endShape();
    shapes.put(name, m1);
    colors.put(name, clr);
    this.alpha.put(name, alpha);
    menus.put(name, menu);
  }
  
  public void setText(String name, String text){
    /*float[] b = buttons.get(name);
    stroke(200);
    textSize(size);
    text(text, (b[0] + 10)/2 - width/4, (b[1] + ((7 * b[4])/8))/2 - height/4, b[2]/2);*/
  // text("popoooo", 0, 0, 0);
    this.text.put(name, text);
  }
  
  public String getText(String name){
    return text.get(name);
  }
  
  public void addText(String name, String l){
    if(text.get(name) == null){
      if(l == null){
        text.put(name, "");
      }else{
        text.put(name, l);
      }
    }else{
      text.put(name, text.get(name) + l);
    }
  }
  
  public void deleteChar(String name){
    if(text.keySet().contains(name)){
      if(text.get(name) != null && text.get(name).length() > 0){
        char[] list = text.get(name).toCharArray();
        char[] list2 = new char[list.length - 1];
        String newStr;
        for(int i = 0; i<list2.length; i++){
          list2[i] = list[i];
        }
        newStr = String.valueOf(list2);
        text.replace(name, newStr);
      }
    }
  }
  
  public void display(String name){
    pushMatrix();
    float[] b = buttons.get(name);
    translate(b[0]/2 - width/4, b[1]/2 - height/4, b[2]/2);
    shape(shapes.get(name));
    popMatrix();
    stroke(200);
    textSize(size);
    if (text.containsKey(name) && text.get(name) != null) text(this.text.get(name), (b[0] + 10)/2 - width/4, (b[1] + ((7 * b[4])/8))/2 - height/4, b[2]/2);
  }
  
  private void otherButtons(String keyy){
    if(!buttonsPressed.containsKey(keyy)){
      buttonsPressed.put(keyy, true);
    }else{
      buttonsPressed.replace(keyy, !buttonsPressed.get(keyy));
    }
  }
  
  public boolean isPressed(String keyy){
   // println(buttons.get("one")[0], buttons.get("one")[1], buttons.get("one")[3], buttons.get("one")[4]);
    if(buttonsPressed.containsKey(keyy)){
      return buttonsPressed.get(keyy);
    }else{
      println("not even there");
      return false;
    }
  }
  
  public String buttonPressed(){
    for(String keyy : buttonsPressed.keySet()){
      if(buttonsPressed.get(keyy)){
        previous[0] = previous[1];
        previous[1] = keyy;
        return keyy;
      }
    }
    return null;
  }
  
  public void checkButtons(float cx, float cy){
   // cx *= 2;
   // cy *= 2;
    for(String keyy : buttons.keySet()){
      if(buttons.get(keyy)[0] < cx && buttons.get(keyy)[0] + buttons.get(keyy)[3] > cx
      && buttons.get(keyy)[1] < cy && buttons.get(keyy)[1] + buttons.get(keyy)[4] > cy
      && menus.get(keyy) == activeMenu){
        otherButtons(keyy);
      }else{
        //buttonsPressed.replace(keyy, false);
      }
    }
  }
  
  public void notPressed(){
    for(String keyy : buttonsPressed.keySet()){
      buttonsPressed.replace(keyy, false);
    }
  }
  
  public void setMenu(String menu){
    activeMenu = menu;
  }
  
  public void setSize(int size){
    this.size = size;
  }
  
  public void print(){
    println("Active:", activeMenu);
    for(String keyy : buttons.keySet()){
      println("  ", keyy, "is in", menus.get(keyy));
    }
  }
  
  public String lastButtonPressed(){
    return previous[0];
  }
  
  //SHAPES ARE ON THE GEOMETRIX ORIGIN
  //THE SCREEN USES ITS OWN ORIGIN, CORRECT IT
}
