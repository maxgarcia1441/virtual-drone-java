import java.net.*;
import java.io.*;
import java.util.Arrays;

class UDP_client{
  private DatagramSocket socket;
  private InetAddress address;
  private Integer port;
  
  private byte[] buf;
  
  private byte[] rawAddress;
  
  private String strAddress;
  private String exception;
  
  public UDP_client() {
    try {
      socket = new DatagramSocket();
      address = InetAddress.getByName("localhost");
    }catch (Exception e) {
      println(e);
   //   e.printStackTrace(); 
    }
  }
  
  public UDP_client(Integer port) {
    try {
      socket = new DatagramSocket();
      address = InetAddress.getByName("localhost");
      this.port = port;
    }catch (Exception e) {
      println(e);
   //   e.printStackTrace(); 
    }
  }
  
  public UDP_client(String strAddress, Integer port) {
    try {
      String[] str = strAddress.split(".", 4);
      byte[] bytes = new byte[4];
      for(int i=0; i<4; i++){
        bytes[i] = (byte)Integer.parseInt(str[i]);
      }
      socket = new DatagramSocket();
      address = InetAddress.getByAddress(bytes);
      this.port = port;
    }catch (Exception e) {
      println(e);
   //   e.printStackTrace(); 
    }
  }
    
  public void send(String msg) {
    try {
      buf = null;
      buf = msg.getBytes();
      DatagramPacket toSend = new DatagramPacket(buf, buf.length, address, port);
      socket.send(toSend);
     // System.gc();
    }catch(Exception e){
    }
  }

  public void close() {
    socket.close();
  }
    
  public void setIPAddress(String strAddress){
    if(strAddress != null) strAddress.replace(" ", "");
    println(strAddress);
    try {
      if(strAddress == null || strAddress.equals("") || strAddress.equals("localhost")){
        address = InetAddress.getByName("localhost");
      }else{
        String[] str = strAddress.split("\\.", -2);
        exception = null;
        byte[] bytes = new byte[4];
        for(int i=0; i<4; i++){
          bytes[i] = (byte)Integer.parseInt(str[i]);
        }
        address = InetAddress.getByAddress(bytes);
      }
      exception = null;
      println(address);
    }catch (Exception e) {
      exception = "Not an IP address";
      println(e);
      println("sadly");
    }
  }
  
  public String getIPAddress(){
    rawAddress = address.getAddress();
    strAddress = String.valueOf(int(rawAddress[0]));
    for(int i=0; i<3; i++) strAddress = strAddress + "." + String.valueOf(int(rawAddress[i]));
    return strAddress;
  }
  
  public void setPort(Integer port){
    this.port = port;
  }
  
  public String getException(){
    return exception;
  }
}
