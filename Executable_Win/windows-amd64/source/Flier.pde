class Flier extends Solid{
  
  public Flier(String name, PShape shape, boolean selection, float mass, float rest){
    super(name, shape, selection , mass, true, rest);
  }
}
