float speed = 10;
Camera camera;
PImage img;
Solids solids = new Solids();
Controls controls = new Controls();
LVControls lvcontrols = new LVControls();
Widgets buttons = new Widgets();
PVector cam_m;
UDP_server[] players = new UDP_server[3];    ////////////////Update this

boolean f = false;
boolean b = false;
boolean r = false;
boolean l = false;
boolean u = false;
boolean d = false;

// Delcaring all shapes
Solid kitten;
Solid cat;
Solid floor;
Solid please;
Solid box1;
Solid box2;

Player drone;
Player drone2, drone3, drone4;

MapOne map;

int selector = 1;
int frames_stat = 0;
int[] mouse_p = new int[2];

float a_XZ, a_XZ_Y;

boolean loaded[] = {false, false, false, false, false};

PImage menu1;

String buttonPressed;
String lastButtonPressed;
String lastButtonPressed2;
String mode;
Integer onlyNumber;

boolean pressed;
boolean released;
int ind;
boolean oneKey;

HashMap<String, Boolean> selectedPlayers;
int t1, t2;
boolean cursorON = false;
int cursorFrames;

String[] messages = new String[3];
String[][] sPorts = new String[2][3];
Integer[][] ports = new Integer[2][3];

String msgToUser;
boolean ok = true;

boolean manualControl = true;
int[] progress = new int[4];
boolean streaming = false;
String[] ips = new String[3];

Boolean options[] = new Boolean[3];

Sphere intro;

float menuAngle = -90;

void keyPressed() {
  //println(keyCode);
  pressed = true;
  if (key == ESC) key = 0;
  if (key == 'a') l = true;
  if (key == 'd') r = true;
  if (key == 'w') f = true;
  if (key == 's') b = true;
  if (keyCode == 38) u = true;
  if (keyCode == 40) d = true;
  if (keyCode == 32) selector++;
  if (selector > solids.nSolids()) selector = 0;
  controls.otherKeys(keyCode);
  if (keyCode == 27) setMode("main");
  controls.otherKeys(key);  ////////////////////////////////////////////////// SWITCH THIS TO KEYS FROM LABVIEW
  onlyNumber = controls.getNumber();
  oneKey(true, false);
}

  
void keyReleased() {
  pressed = false;
  if (key == 'a') l = false;
  if (key == 'd') r = false;
  if (key == 'w') f = false;
  if (key == 's') b = false;
  if (keyCode == 38) u = false;
  if (keyCode == 40) d = false;
  controls.otherKeys(keyCode);
 // if (controls.isPressed(
 oneKey(false, true);
}
  
void mouseWheel(MouseEvent event) {
  //solids.get("cat").move(0, event.getCount() * 15, 0);  //up down
  if(event.getCount() >= 0){
    camera.zoom(1);
  }else{
    camera.zoom(-1);
  }
}

void mouseClicked() {
  buttons.notPressed();
  buttons.checkButtons(mouseX, mouseY);
  buttonPressed = buttons.buttonPressed();
  lastButtonPressed2 = lastButtonPressed;
  if(buttonPressed != null) lastButtonPressed = buttonPressed;
 /* if(buttonPressed != null){
    switch(buttonPressed){
      case "one":
        mode = 1;
        break;
      case "three":
        exit();
      case "two":
        mode = 2;
    }
  }*/
}

void setMode(String mode){
  this.mode = mode;
  buttons.setMenu(mode);
}

void rotateTo(float a_XZ, float a_XZ_Y){
 // rotateY(radians(-this.a_XZ));
 // rotateZ(radians(-this.a_XZ_Y));
  this.a_XZ = a_XZ;
  this.a_XZ_Y = a_XZ_Y;
  rotateY(radians(-this.a_XZ));
  rotateZ(radians(-this.a_XZ_Y + 90));
}

void loadMenu(){
  intro = new Sphere("outside2inv.jpg", 0, 0, 0);
  loaded[4] = true;
}

void loadMap(){
  map = new MapOne();
  map.setNExpected(49);
  map.loadSpheres();
  loaded[0] = true;
}

void loadPlayers(){
  drone = new Player("drone");
  progress[1] += 1;
  drone2 = new Player("drone2");
  progress[1] += 1;
  drone3 = new Player("drone3");
  progress[1] += 1;
  drone4 = new Player("drone4");
  progress[1] += 1;
  loaded[1] = true;
  drone.initializeServer(8888, 16, 40);
  progress[2] += 1;
  drone2.initializeServer(8887, 16, 40);
  progress[2] += 1;
  drone3.initializeServer(8886, 16, 40);
  progress[2] += 1;
  drone4.initializeServer(8885, 16, 40);
  progress[2] += 1;
 // drone.setCamera(camera);
  drone.tagOFF();
  drone2.setCamera(camera);
  drone3.setCamera(camera);
  drone4.setCamera(camera);
  
  lvcontrols.addLVControl(drone2.getServer());
  lvcontrols.addLVControl(drone3.getServer());
  lvcontrols.addLVControl(drone4.getServer());
  
  players[0] = drone2.getServer();
  players[1] = drone3.getServer();
  players[2] = drone4.getServer();
  
  drone.initializeClient();
  drone2.initializeClient();
  drone3.initializeClient();
  drone4.initializeClient();
  
  drone.getUDP_client().setPort(9988);
  
  loaded[2] = true;
  
  loadObjects();
}

void loadObjects(){
  cat = new Solid("cat", createShape(BOX, 30), true, 40, true, 0.8);
  progress[3] += 10;
  floor = new Solid("floor", createShape(BOX, 6000), true, 1000000000, false, 0.7);
  progress[3] += 10;
  please = new Solid("please", createShape(BOX, 45), true, 100, true, 0.7);
  progress[3] += 10;
  box2 = new Solid("box2", createShape(BOX, 20), true, 20, true, 0.7);
  progress[3] += 10;
  box1 = new Solid("box1", createShape(BOX, 20), true, 20, true, 0.7);
  progress[3] += 10;
  kitten = new Solid("kitten", loadShape("cat.obj"), false, 30, true, 0.9);
  progress[3] += 10;
  
  solids.add(drone.getSolid());
  solids.add(drone2.getSolid());
  solids.add(drone3.getSolid());
  solids.add(drone4.getSolid());
  progress[3] += 10;
  
  solids.add(kitten);
  solids.add(cat);
  solids.add(floor);
  solids.add(please);
  solids.add(box2);
  solids.add(box1);
  progress[3] += 10;
  
  solids.get("please").moveTo(40, -solids.get("please").getHeight()/2, - 70);
  solids.get("kitten").moveTo(-30, -solids.get("kitten").getHeight()/2, - 110);
  solids.get("cat").moveTo(0, -solids.get("cat").getHeight()/2, 0);
  solids.get("box2").moveTo(-40, -solids.get("box2").getHeight()/2, - 80);
  solids.get("box1").moveTo(-40, -solids.get("box1").getHeight()/2, - 140);
  solids.get("floor").moveTo(camera.getX(), solids.get("floor").getHeight()/2, camera.getZ());
  solids.get("drone").moveTo(500, solids.get("drone").getHeight()/2, 500);
  solids.get("drone2").moveTo(250, solids.get("drone2").getHeight()/2, 250);
  solids.get("drone3").moveTo(250, solids.get("drone3").getHeight()/2, 500);
  solids.get("drone4").moveTo(500, solids.get("drone4").getHeight()/2, 250);
  progress[3] += 10;
  
  solids.get("drone").setObjPosition(-200.0, 185.0, 50.0);
  
  solids.get("drone").shape.rotateX(radians(90));
  solids.get("drone").shape.rotateY(radians(270));
  solids.get("drone2").shape.rotateX(radians(90));
  solids.get("drone2").shape.rotateY(radians(270));
  solids.get("drone3").shape.rotateX(radians(90));
  solids.get("drone3").shape.rotateY(radians(270));
  solids.get("drone4").shape.rotateX(radians(90));
  solids.get("drone4").shape.rotateY(radians(270));
  solids.get("kitten").shape.rotateX(radians(90));
  solids.get("kitten").shape.rotateY(radians(270));
  progress[3] += 10;
  
  //drone.setFlight(true);
  loaded[3] = true;
}

void loadInBackground(){
  map.loadInBackGround();
}

void oneKey(boolean pressed, boolean released){
  if(pressed) ind = 1;
  if(released && ind == 1) ind = 2;
}

void msg1(){   ///////////////////////// This is so processing can receive messages from all players simultanneously
  messages[0] = drone2.getServer().getMessage();
}

void msg2(){
  messages[1] = drone3.getServer().getMessage();
}

void msg3(){
  messages[2] = drone4.getServer().getMessage();
}

void getMessages(){
  if(selectedPlayers.get("2_9")) thread("msg1");
  if(selectedPlayers.get("2_10")) thread("msg2");
  if(selectedPlayers.get("2_11")) thread("msg3");
}

void sendMessages(){
  //if((millis() / 100) % 10 == 0){
  if(loaded[2] && streaming == true){
    if(selectedPlayers.get("2_9")){
      if(options[0]) drone2.sendPosition();
      if(options[1]) drone2.sendVelocity();
      if(options[2]) drone2.sendAcceleration();
      drone2.sendActive();
    }
    if(selectedPlayers.get("2_10")){
      if(options[0]) drone3.sendPosition();
      if(options[1]) drone3.sendVelocity();
      if(options[2]) drone3.sendAcceleration();
      drone3.sendActive();
    }
    if(selectedPlayers.get("2_11")){
      if(options[0]) drone4.sendPosition();
      if(options[1]) drone4.sendVelocity();
      if(options[2]) drone4.sendAcceleration();
      drone4.sendActive();
    }
  }
//  }
}

void mcft(){
  menuAngle -= 0.02;
  rotateY(radians(menuAngle));
  if(loaded[4]) intro.display();
}

void game(){
  
  camera(camera.getX(), camera.getY(), camera.getZ(), // eyeX, eyeY, eyeZ
         camera.getO_x(), camera.getO_y(), camera.getO_z(), // centerX, centerY, centerZ
         0.0, 1.0, 0.0); // upX, upY, upZ
         
  lights();
 // pointLight(255, 255, 255, 100, -100, 100);
  background(0);
  
  camera.rotateO(0, 0);
  if(mouse_p[0] == mouseX && mouse_p[1] == mouseY){
    frames_stat++;
  }else{
    frames_stat = 0;
  }
  if(frames_stat < 25) camera.rotateO(controls.mouse_to_angle(mouseX, mouseY).x, controls.mouse_to_angle(mouseX, mouseY).y);
  mouse_p[0] = mouseX;
  mouse_p[1] = mouseY;
  
  pushMatrix();
  
  //thread("getMessage");
  
  solids.get("cat").shape.setStroke(0);
  translate(solids.get("cat").getX(), solids.get("cat").getY(), solids.get("cat").getZ());
  rotateTo(solids.get("cat").getA_XZ(), solids.get("cat").getA_XZ_Y());
  shape(solids.get("cat").shape);
  // Phone Orientation Control
  /*float[] data_float = player1.dataToString(player1.getMessage());
  if(data_float.length > 0 && data_float[0] != 0){
    PVector phone1 = new PVector();
    phone1.x = data_float[14] * solids.get("cat").mass;
    phone1.y = data_float[15] * solids.get("cat").mass;
    phone1.z = data_float[16] * solids.get("cat").mass;
    solids.get("cat").push(phone1);
  */
  popMatrix(); 
  
  stroke(255);
  line(-100, 0, 0, 100, 0, 0);
  line(0, -100, 0, 0, 100, 0);
  line(0, 0, -100, 0, 0, 100);
  
  //solids.get("floor").display(color(234, 10, 82), 0, 0, 0);
  
  solids.get("please").display(color(7, 140, 82), 0, 0, 0);
  solids.get("box1").display(color(255,190,200), 0, 0, 0);
  solids.get("box2").display(color(255,190,200), 0, 0, 0);
  solids.get("kitten").display(color(150, 140, 0), 0, 0, 0);
  drone.display();
  drone2.display();
  drone3.display();
  drone4.display();
  
//  println(selector - 1);
  drone.setFlight(false);
  drone2.setFlight(false);
  drone3.setFlight(false);
  drone4.setFlight(false);
  if(selector == 1) drone.setFlight(true);
  if(selector == 2) drone2.setFlight(true);
  if(selector == 3) drone3.setFlight(true);
  if(selector == 4) drone4.setFlight(true);
  //solids.gravity(10);
  controls.setKeys(f, b, r, l, u, d);
  
  if(selector == 0){
    cam_m = controls.moveCam(camera);
    camera.moveC(cam_m.x, cam_m.y, cam_m. z);
    textSize(128);
    text("select a character", 0, 0, 0);
  }else{
    // println(solids.index_to_name(selector - 1));
    camera.rotateO(controls.keys_to_angle().x, controls.keys_to_angle().y);
    if(manualControl){
      solids.get(solids.index_to_name(selector - 1)).push(controls.moveUseCam(camera));
   // solids.get(solids.index_to_name(selector - 1)).rotateTo(camera.getA_XZ(), camera.getA_XZ_Y() - 90);
   //solids.get(solids.index_to_name(selector - 1)).rotateTo(camera.getA_XZ(), 0);
      solids.get(solids.index_to_name(selector - 1)).rotateTo(camera.getPastXZ(), 0);
    }else{
      solids.get(solids.index_to_name(selector - 1)).push(controls.moveUseCam(camera));
      solids.get(solids.index_to_name(selector - 1)).rotateTo(camera.getPastXZ(), 0);
      
      if(drone2.isConnected()){
        lvcontrols.LVotherKeys("drone2");
        solids.get("drone2").push(lvcontrols.moveUseCam("drone2", camera));
      }
      if(drone3.isConnected()){
        lvcontrols.LVotherKeys("drone3");
        solids.get("drone3").push(lvcontrols.moveUseCam("drone3", camera));
      }
      if(drone4.isConnected()){
        lvcontrols.LVotherKeys("drone4");
        solids.get("drone4").push(lvcontrols.moveUseCam("drone4", camera));
      }
  
      /*if(drone2.isConnected()) solids.get("drone2").push(lvcontrols.moveUseCam("drone2", camera));
      if(drone3.isConnected()) solids.get("drone3").push(lvcontrols.moveUseCam("drone3", camera));
      if(drone4.isConnected()) solids.get("drone4").push(lvcontrols.moveUseCam("drone4", camera));*/
    }
    camera.follow(solids.get(solids.index_to_name(selector - 1)));
    if(loaded[0]){
      map.setSolid(solids.get(solids.index_to_name(selector - 1)));
      map.displayClosestSphere();
      solids.containSolids(map.getClosestSphere());
      //map.displayAll();
    }else{
      textSize(128);
      text("the world is LOADING...", 0, 0, 0);
    }
  }
//  map.orderSpheres();
  solids.getSolids().forEach(s -> s.resetOr());
  solids.checkCollisions();
}

void menu1(){
  /*camera(camera.getX(), camera.getY(), camera.getZ(), // eyeX, eyeY, eyeZ
         camera.getO_x(), camera.getO_y(), camera.getO_z(), // centerX, centerY, centerZ
         0.0, 1.0, 0.0); // upX, upY, upZ*/
         
  camera(0, 0, 500,
  0, 0, 0,
  0, 1, 0);
  lights();
 // pointLight(255, 255, 255, 100, -100, 100);
  background(50);
  
  textSize(100);
  fill(255);
  //text("Main menu", -230, -200, 0);
  buttons.setSize(111);
  buttons.display("mMenu");
  buttons.setSize(60);
  
  buttons.display("one");
  buttons.display("two");
  buttons.display("three");
  
  buttons.setText("mMenu", "Main Menu");
  buttons.setText("one", "Start");
  buttons.setText("two", "Settings");
  buttons.setText("three", "Exit Game");
  
  if(buttonPressed != null){
    switch(buttonPressed){
      case "one":
        setMode("game");
        break;
      case "three":
        exit();
      case "two":
        setMode("settings");
    }
  }
 // println(player1.getKeysPressed());
  buttons.notPressed();
  buttonPressed = buttons.buttonPressed();
  
  mcft();
}

void writeNumbersTo(String name){
  if(ind == 2){
    if(keyCode == 8){
      buttons.deleteChar(name);
    }else{
      if(String.valueOf(onlyNumber) != "null") buttons.addText(name, String.valueOf(onlyNumber));
    }
  }
}

void writeTextTo(String name){
  if(ind == 2){
    if(keyCode == 8){
      buttons.deleteChar(name);
    }else{
      buttons.addText(name, String.valueOf(key));
    }
  }
}

void addCursor(){
  //if(int(millis() / 1000) 
  String b = lastButtonPressed;
  if(t1 != t2){
    cursorON = true;
    buttons.addText(b, "|");
    delay(150);
    cursorON = false;
    buttons.deleteChar(b);
  }
}

void menu2(){
  camera(0, 0, 500,
  0, 0, 0,
  0, 1, 0);
  background(40);
  
  textSize(70);
  fill(255);
  text("UDP server", -150, -200, 0);
  
  buttons.setSize(35);
  
  buttons.display("2one");
  buttons.display("2two");
  buttons.display("2three");
  buttons.display("2four");
  buttons.display("2_5");
  buttons.display("2_6");
  buttons.display("2_7");
  buttons.display("2_8");
  if(ok){
    buttons.display("name");
    buttons.display("port");
  }else{
    buttons.display("2_12");
  }
  buttons.display("msg1");
  buttons.display("msg2");
  buttons.display("msg3");
  buttons.display("manual");
  buttons.display("next");
  buttons.display("text");
  
  buttons.setText("2four", "Back");
  
  buttons.setSize(28);
  if(loaded[3]){
    buttons.setText("2one", drone2.getDisplayName());
    buttons.setText("2two", drone3.getDisplayName());
    buttons.setText("2three", drone4.getDisplayName());
  }else{
    buttons.setText("2one", "P1 Loading");
    buttons.setText("2two", "P2 Loading");
    buttons.setText("2three", "P3 Loading");
  }
  buttons.setText("2_8", "Save");
  buttons.setText("2_12", msgToUser);
  buttons.setText("msg1", messages[0]);
  buttons.setText("msg2", messages[1]);
  buttons.setText("msg3", messages[2]);
  buttons.setText("next", "Next");
  buttons.setText("name", "Name (Active)");
  buttons.setText("port", "Port");
  buttons.setText("text", "Incoming data");
  if(manualControl){
    buttons.setText("manual", "   LabVIEW Control: OFF");
  }else{
    buttons.setText("manual", "   LabVIEW Control: ON");
  }
  
  
  if(lastButtonPressed != null){
    switch(lastButtonPressed){
      case "2four":
        setMode("main");
        break;
      case "2one":
        if(!selectedPlayers.get("2_9")) buttons.setText("2_5", "");
        thread("addCursor");
        if(!cursorON){
          writeTextTo("2one");
          if(loaded[3]) drone2.setDisplayName(buttons.getText("2one"));
        }
        break;
      case "2two":
        if(!selectedPlayers.get("2_10")) buttons.setText("2_6", "");
        thread("addCursor");
        if(!cursorON){
          writeTextTo("2two");
          if(loaded[3]) drone3.setDisplayName(buttons.getText("2two"));
        }
        break;
      case "2three":
        if(!selectedPlayers.get("2_11")) buttons.setText("2_7", "");
        thread("addCursor");
        if(!cursorON){
          writeTextTo("2three");
          if(loaded[3]) drone4.setDisplayName(buttons.getText("2three"));
        }
        break;
      case "2_5":
        if(selectedPlayers.get("2_9")){
          thread("addCursor");
          if(!cursorON) writeNumbersTo("2_5");
        }else{
          buttons.setText("2_5", "");
        }
        buttons.setText("2_8", "Save");
        break;
      case "2_6":
        if(selectedPlayers.get("2_10")){
          thread("addCursor");
          if(!cursorON) writeNumbersTo("2_6");
        }else{
          buttons.setText("2_6", "");
        }
        buttons.setText("2_8", "Save");
        break;
      case "2_7":
        if(selectedPlayers.get("2_11")){ 
          thread("addCursor");
          if(!cursorON) writeNumbersTo("2_7");
        }else{
          buttons.setText("2_7", "");
        }
        buttons.setText("2_8", "Save");
        break;
      case "2_8":
        buttons.setText("2_8", "Save-d");
        break;
    }
  }
  if(buttonPressed != null){
    println(buttonPressed);
    ok = true;
    switch(buttonPressed){
      case "2one":
        selectedPlayers.replace("2_9", !selectedPlayers.get("2_9"));
        if(!selectedPlayers.get("2_9")) drone2.getServer().close();
        break;
      case "2two":
        selectedPlayers.replace("2_10", !selectedPlayers.get("2_10"));
        if(!selectedPlayers.get("2_10")) drone3.getServer().close();
        break;
      case "2three":
        selectedPlayers.replace("2_11", !selectedPlayers.get("2_11"));
        if(!selectedPlayers.get("2_11")) drone4.getServer().close();
        break;
      case "2_8":
       // if(selectedPlayers.get("2_9")) player1.changePort();
       if(loaded[3]){
        sPorts[0][0] = buttons.getText("2_5");
        sPorts[0][1] = buttons.getText("2_6");
        sPorts[0][2] = buttons.getText("2_7");
        if(cursorON && lastButtonPressed2 == "2_5")  sPorts[0][0] =  sPorts[0][0].substring(0,  sPorts[0][0].length() - 1);
        if(cursorON && lastButtonPressed2 == "2_6")  sPorts[0][1] =  sPorts[0][1].substring(0,  sPorts[0][1].length() - 1);
        if(cursorON && lastButtonPressed2 == "2_7")  sPorts[0][2] =  sPorts[0][2].substring(0,  sPorts[0][2].length() - 1);
        for(int i=0; i<3; i++){
          if(sPorts[0][i] != null && !sPorts[0][i].equals("")) ports[0][i] = Integer.valueOf(sPorts[0][i]);
        }
        for(int i=0; i<3; i++){
          players[i].changePort(ports[0][i]);
          if(players[i].exception()){
            msgToUser = players[i].getName() + " port is already in use";
            ok = false;
          }
        }
       }else{
         msgToUser = "Loading still in progress";
         ok = false;
       }
        if(ok) msgToUser = "";
        break;
      case "manual":
        manualControl = !manualControl;
        println(manualControl);
        break;
      case "next":
        setMode("client");
        break;
    }
  }
  
  for(String s : selectedPlayers.keySet()){
    if(selectedPlayers.get(s)) buttons.display(s);
  }
  buttons.notPressed();
  buttonPressed = buttons.buttonPressed();
}

void menu3(){
  camera(0, 0, 500,
  0, 0, 0,
  0, 1, 0);
  background(40);
  
  textSize(70);
  fill(255);
  text("UDP client", -150, -200, 0);
  
  buttons.display("back");
  buttons.setSize(25);
  buttons.display("ip1");
  buttons.display("ip2");
  buttons.display("ip3");
  buttons.setSize(37);
  buttons.display("p1");
  buttons.display("p2");
  buttons.display("p3");
  if(ok){
    buttons.display("ip");
    buttons.display("port");
  }else{
    buttons.display("2_12");
  }
  buttons.display("name1");
  buttons.display("name2");
  buttons.display("name3");
  buttons.display("save");
  buttons.display("streaming");
  buttons.display("position");
  buttons.display("velocity");
  buttons.display("acceleration");
  buttons.display("options");
  for(String s : selectedPlayers.keySet()) if(selectedPlayers.get(s)) buttons.display(s);
  
  buttons.setText("2_12", msgToUser);
  buttons.setText("back", "Back");
  buttons.setText("ip", "IP address");
  buttons.setText("port", "Port");
  buttons.setText("save", "Save");
  buttons.setText("options", "Data");
  if(options[0]){
    buttons.setText("position", "Position: ON");
  }else{
    buttons.setText("position", "Position: OFF");
  }
  if(options[1]){
    buttons.setText("velocity", "Velocity: ON");
  }else{
    buttons.setText("velocity", "Velocity: OFF");
  }
  if(options[2]){
    buttons.setText("acceleration", "Acceleration: ON");
  }else{
    buttons.setText("acceleration", "Acceleration: OFF");
  }
  if(loaded[3]){
    buttons.setText("name1", drone2.getDisplayName());
    buttons.setText("name2", drone3.getDisplayName());
    buttons.setText("name3", drone4.getDisplayName());
  }else{
    buttons.setText("name1", "P1 Loading");
    buttons.setText("name2", "P2 Loading");
    buttons.setText("name3", "P3 Loading");
  }
  if(streaming){
    buttons.setText("streaming", "    Streaming: ON");
  }else{
    buttons.setText("streaming", "    Streaming: OFF");
  }
  
  buttons.setSize(35);
  if(lastButtonPressed != null){
    switch(lastButtonPressed){
      case "ip1":
        if(selectedPlayers.get("2_9")){
          thread("addCursor");
          if(!cursorON){
            writeTextTo("ip1");
            if(loaded[3]);
          }
        }else{
          buttons.setText("ip1", "");
        }
        break;
      case "ip2":
        if(selectedPlayers.get("2_10")){
          thread("addCursor");
          if(!cursorON){
            writeTextTo("ip2");
            if(loaded[3]);
          }
        }else{
          buttons.setText("ip2", "");
        }
        break;
      case "ip3":
        if(selectedPlayers.get("2_11")){
          thread("addCursor");
          if(!cursorON){
            writeTextTo("ip3");
            if(loaded[3]);
          }
        }else{
          buttons.setText("ip3", "");
        }
        break;
      case "p1":
        if(selectedPlayers.get("2_9")){
          thread("addCursor");
          if(!cursorON) writeNumbersTo("p1");
        }else{
          buttons.setText("p1", "");
        }
        break;
      case "p2":
        if(selectedPlayers.get("2_10")){
          thread("addCursor");
          if(!cursorON) writeNumbersTo("p2");
        }else{
          buttons.setText("p2", "");
        }
        break;
      case "p3":
        if(selectedPlayers.get("2_11")){ 
          thread("addCursor");
          if(!cursorON) writeNumbersTo("p3");
        }else{
          buttons.setText("p3", "");
        }
        break;
      case "save":
        buttons.setText("save", "Save-d");
    }
  }
  if(buttonPressed != null){
    ok = true;
    switch(buttonPressed){
      case "back":
        setMode("settings");
        break;
      case "save":
        if(loaded[3]){
          sPorts[1][0] = buttons.getText("p1");
          sPorts[1][1] = buttons.getText("p2");
          sPorts[1][2] = buttons.getText("p3");
          ips[0] = buttons.getText("ip1");
          ips[1] = buttons.getText("ip2");
          ips[2] = buttons.getText("ip3");
          if(cursorON){
            switch(lastButtonPressed){
              case "p1":
                if(selectedPlayers.get("2_9")) sPorts[1][0] =  sPorts[1][0].substring(0,  sPorts[1][0].length() - 1);
              case "p2":
                if(selectedPlayers.get("2_10")) sPorts[1][1] =  sPorts[1][1].substring(0,  sPorts[1][1].length() - 1);
              case "p3":
                if(selectedPlayers.get("2_11")) sPorts[1][2] =  sPorts[1][2].substring(0,  sPorts[1][2].length() - 1);
              case "ip1":
                if(selectedPlayers.get("2_9")) ips[0] = ips[0].substring(0,  ips[0].length() - 1);
              case "ip2":
                if(selectedPlayers.get("2_10")) ips[1] = ips[1].substring(0,  ips[1].length() - 1);
              case "ip3":
                if(selectedPlayers.get("2_11")) ips[2] = ips[2].substring(0,  ips[2].length() - 1);
            }
          }
          for(int i=0; i<3; i++){
            if(sPorts[1][i] != null && !sPorts[1][i].equals("")) ports[1][i] = Integer.valueOf(sPorts[1][i]);
          }
          if(selectedPlayers.get("2_9")){
            drone2.getUDP_client().setPort(ports[1][0]);
            drone2.getUDP_client().setIPAddress(ips[0]);
            if(drone2.getException() != null){
              ok = false;
              msgToUser = "That's not an IP address";
            }
          }
          if(selectedPlayers.get("2_10")){
            drone3.getUDP_client().setPort(ports[1][1]);
            drone3.getUDP_client().setIPAddress(ips[1]);
            if(drone2.getException() != null){
              ok = false;
              msgToUser = "That's not an IP address";
            }
          }
          if(selectedPlayers.get("2_11")){
            drone4.getUDP_client().setPort(ports[1][2]);
            drone4.getUDP_client().setIPAddress(ips[2]);
            if(drone4.getException() != null){
              ok = false;
              msgToUser = "That's not an IP address";
            }
          }
        }
        break;
      case "streaming":
        streaming = !streaming;
        break;
      case "position":
        options[0] = !options[0];
        break;
      case "velocity":
        options[1] = !options[1];
        break;
      case "acceleration":
        options[2] = !options[2];
        break;
    }
  }
  
  buttons.notPressed();
  buttonPressed = buttons.buttonPressed();
}

void setup() {
  //img = loadImage("the_room.jpg");
  //menu1 = loadImage("stars_2.jpg");
  size(2048, 1152, P3D);
  surface.setLocation(10, 10);
  //size(1000, 500, P3D);
  //fullScreen(P3D);
  setMode("main");
  camera = new Camera();
  a_XZ = 0;
  a_XZ_Y = 0;
  fill(204);
  selectedPlayers = new HashMap<>();
  
  for(int o = 0; o < 3; o++) options[o] = false;
  
  buttons.addButton("main", "mMenu", color(28, 51, 114), 255, width/4, 0.5*(height/5), 0, width/2, height/6);
  buttons.addButton("main", "one", color(182, 0, 108), 255, width/3, 2*(height/5), 0, width/3, height/10);
  buttons.addButton("main", "two", color(28, 51, 114), 255, width/3, 3*(height/5), 0, width/3, height/10);
  buttons.addButton("main", "three", color(28, 51, 114), 255, width/3, 4*(height/5), 0, width/3, height/10);
  
  buttons.addButton("settings", "name", color(179, 169, 0), 0, width/3 - width/8, 4*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("settings", "port", color(179, 169, 0), 0, (5*width)/9 - width/8, 4*(height/10) - height/20, 0, width/8, height/16);
  
  buttons.addButton("settings", "2one", color(179, 169, 0), 255, width/3 - width/8, 5*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("settings", "2two", color(179, 169, 0), 255, width/3 - width/8, 6*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("settings", "2three", color(179, 169, 0), 255, width/3 - width/8, 7*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("settings", "2four", color(182, 0, 108), 255, width/3 - width/8, 3*(height/10) - height/20, 0, width/6, height/16);
  
  buttons.addButton("settings", "2_5", color(179, 169, 0), 255, (5*width)/9 - width/8, 5*(height/10) - height/20, 0, width/8, height/16);
  buttons.addButton("settings", "2_6", color(179, 169, 0), 255, (5*width)/9 - width/8, 6*(height/10) - height/20, 0, width/8, height/16);
  buttons.addButton("settings", "2_7", color(179, 169, 0), 255, (5*width)/9 - width/8, 7*(height/10) - height/20, 0, width/8, height/16);
  
  selectedPlayers.put("2_9", true);
  selectedPlayers.put("2_10", false);
  selectedPlayers.put("2_11", false);
  
  buttons.addButton("settings", "2_9", color(159, 218, 164), 255, (9*width)/18 - width/8, 5*(height/10) - height/20, 0, width/32, height/16);
  buttons.addButton("settings", "2_10", color(159, 218, 164), 255, (9*width)/18 - width/8, 6*(height/10) - height/20, 0, width/32, height/16);
  buttons.addButton("settings", "2_11", color(159, 218, 164), 255, (9*width)/18 - width/8, 7*(height/10) - height/20, 0, width/32, height/16);
  
  buttons.addButton("settings", "2_8", color(182, 0, 108), 255, width/3 - width/8, 9*(height/10) - height/20, 0, width/6, height/16);
  
  buttons.addButton("settings", "2_12", color(255, 0, 0), 255, width/3 - width/8, 4*(height/10) - height/20, 0, (7 * width)/18, height/16);
  buttons.addButton("settings", "text", color(28, 51, 114), 0, (6.5*width)/9 - width/8, 4*(height/10) - height/20, 0, width/8, height/16);
  
  buttons.addButton("settings", "msg1", color(28, 51, 114), 180, (6.5*width)/9 - width/8, 5*(height/10) - height/20, 0, width/3.5, height/16);
  buttons.addButton("settings", "msg2", color(28, 51, 114), 180, (6.5*width)/9 - width/8, 6*(height/10) - height/20, 0, width/3.5, height/16);
  buttons.addButton("settings", "msg3", color(28, 51, 114), 180, (6.5*width)/9 - width/8, 7*(height/10) - height/20, 0, width/3.5, height/16);
  
  buttons.addButton("settings", "manual", color(179, 169, 0), 255, width/3 - width/8, 8*(height/10) - height/20, 0, (2.085 * width)/6, height/16);
  buttons.addButton("settings", "next", color(182, 0, 108), 255, (5*width)/9 - width/8, 3*(height/10) - height/20, 0, width/8, height/16);
  buttons.addButton("client", "back", color(182, 0, 108), 255, width/3 - width/8, 3*(height/10) - height/20, 0, width/6, height/16);
  
  buttons.addButton("client", "ip1", color(179, 169, 0), 255, width/3  - width/8, 5*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("client", "ip2", color(179, 169, 0), 255, width/3 - width/8, 6*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("client", "ip3", color(179, 169, 0), 255, width/3 - width/8, 7*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("client", "p1", color(179, 169, 0), 255, (5*width)/9 - width/8, 5*(height/10) - height/20, 0, width/8, height/16);
  buttons.addButton("client", "p2", color(179, 169, 0), 255, (5*width)/9 - width/8, 6*(height/10) - height/20, 0, width/8, height/16);
  buttons.addButton("client", "p3", color(179, 169, 0), 255, (5*width)/9 - width/8, 7*(height/10) - height/20, 0, width/8, height/16);
  buttons.addButton("client", "ip", color(179, 169, 0), 0, width/3 - width/8, 4*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("client", "port", color(179, 169, 0), 0, (5*width)/9 - width/8, 4*(height/10) - height/20, 0, width/8, height/16);
  
  buttons.addButton("client", "name1", color(179, 169, 0), 0, width/6 - width/8, 5*(height/10) - height/20, 0, width/8, height/16);
  buttons.addButton("client", "name2", color(179, 169, 0), 0, width/6 - width/8, 6*(height/10) - height/20, 0, width/8, height/16);
  buttons.addButton("client", "name3", color(179, 169, 0), 0, width/6 - width/8, 7*(height/10) - height/20, 0, width/8, height/16);
  
  buttons.addButton("client", "save", color(182, 0, 108), 255, width/3 - width/8, 9*(height/10) - height/20, 0, width/6, height/16);
  buttons.addButton("client", "streaming", color(179, 169, 0), 255, width/3 - width/8, 8*(height/10) - height/20, 0, (2.085 * width)/6, height/16);
  
  buttons.addButton("client", "options", color(28, 51, 114), 0, (6.5*width)/9 - width/8, 4*(height/10) - height/20, 0, width/5, height/16);
  buttons.addButton("client", "position", color(182, 0, 108), 180, (6.5*width)/9 - width/8, 5*(height/10) - height/20, 0, width/3.5, height/16);
  buttons.addButton("client", "velocity", color(182, 0, 108), 180, (6.5*width)/9 - width/8, 6*(height/10) - height/20, 0, width/3.5, height/16);
  buttons.addButton("client", "acceleration", color(182, 0, 108), 180, (6.5*width)/9 - width/8, 7*(height/10) - height/20, 0, width/3.5, height/16);
  
  thread("loadMenu");
  thread("loadMap");
  thread("loadPlayers");
}

void draw() {
  t1 = t2;
  t2 = int(millis() / 1000);
  if(loaded[2]){
    getMessages();
    if((millis() / 100) % 10 == 0) thread("sendMessages");
   //thread("sendMessages");
  }
  //  println("t1:", t1, "t2", t2);
  switch(mode){
   case "main":
     menu1();
     break;
   case "game":
     if(loaded[0] && loaded[1] && loaded[2] && loaded[3]){
     game();
     }else{
       textSize(85);
       background(0);
       int x = int((millis()/1000)) % 4;
       progress[0] = int(map.getProgress());
       switch(x){
         case 0:
           text("the world is LOADING   ", - 500, -150, 0);
           break;
         case 1:
           text("the world is LOADING.  ", - 500, -150, 0);
           break;
         case 2:
           text("the world is LOADING.. ", - 500, -150, 0);
           break;
         case 3:
           text("the world is LOADING...", - 500, -150, 0);
           break;
       }
       textSize(60);
       text("Loading 360 Spheres " + progress[0] + "%", -350, -50, 0);
       text("Drones ready to fly " + progress[1] + "/4", -350, 50, 0);
       text("Initializing UDP " + progress[2] + "/4", -350, 150, 0);
       text("Other elements " + progress[3] + "%", -350, 250, 0);
     }
     break;
   case "settings":
     menu2();
     break;
   case "client":
     menu3();
  }
  if(ind == 2){
    ind = 0;
  }
}


//SPHERES CAN ONLY BE DISPLAYED OUTSIDE OF A THREAD...
//CREATE A CLASS FOR MENUS
//ONLY PRESS BUTTONS THAT ARE BEING DISPLAYED}
