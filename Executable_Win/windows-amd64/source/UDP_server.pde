import java.net.*;
import java.io.*;
import java.util.Arrays;

class UDP_server {
  private DatagramSocket socket;
  //private DatagramPacket packet;
  
  private int timeout;
  private int c;
  private String received;
  private int buffer_size;
  
  private int port;
 // private byte[] buf;
  
  private float[] fallback = new float[16];
  private boolean PortBusy;
  private String name;
  
  private byte[] buf;
  
  public UDP_server(String name, int port, int buffer_size, int timeout){
    this.name = name;
    this.port = port;
   // buf = new byte[buffer_size];
    this.timeout = timeout;
    this.buffer_size = buffer_size;
    buf = new byte[buffer_size];
    c = 50;
  }
  
  public String[] getKeysPressed(){
    if(received != null){
      String keys[] = received.split(",", -2);
      for(int i=0; i<keys.length - 1; i++){
        keys[i] = keys[i].replaceAll(" ", "");
      }
      return keys;
    }else{
      String keys[] = null;
      return keys;
    }
  }
  
  public void initialize(){
    try {
      socket = new DatagramSocket(port); // Set your port here
      socket.setSoTimeout(timeout);
      PortBusy = false;
    }
    catch (Exception e) {
      PortBusy = true;
   //   e.printStackTrace(); 
    }
  }
  
  public boolean exception(){
    return PortBusy;
  }
  
  public void changePort(Integer port){
    if(port != null){
      if(!PortBusy) socket.close();
      try {
        buf = null;
        buf = new byte[buffer_size];
        socket = new DatagramSocket(port); // Set your port here
        socket.setSoTimeout(timeout);
        PortBusy = false;
        println("port succesfully changed");
      }
      catch (Exception e) {
        PortBusy = true;
        //e.printStackTrace(); 
        println(e.getMessage());
      }
    }
  }
  
  public String getMessage(){
    try {
      byte[] buf = new byte[buffer_size];
      DatagramPacket packet = new DatagramPacket(buf, buf.length);
      socket.receive(packet);
      InetAddress address = packet.getAddress();
      int port_r = packet.getPort();
      packet = new DatagramPacket(buf, buf.length, address, port_r);

     // println(Arrays.toString(buf));
      byte[] bytes = packet.getData();
      //String received = new String(packet.getData(), 0, packet.getLength());
     // String received = new String(packet.getData());
     // received = received.replace("\n", "");
      int index = 0;
      for(int i = 0; i<bytes.length; i++){
        if((char)bytes[i] == '\n'){
          index = i;
          break;
        }
      }
      byte[] newS = Arrays.copyOfRange(bytes, 0, index + 1);
      String received = new String(newS);
      c = 0;
      
      this.received = received.strip();
      return received.strip();
    } catch (IOException e) {
    //  e.printStackTrace();
     // println("It timed out or something");
     // println(e.getMessage());
      c += 1;
      return null;
    } catch (Exception e){
      c += 1;
      return null;
    }
  }
  
  public boolean isConnected(){
    if(c >= 50){
      return false;
    }else{
      return true;
    }
  }
  
  public String getName(){
    return name;
  }
  
  public void updateFallback(float[] fallback){
    this.fallback = fallback;
  }
  public float[] getFallback(){
    return fallback;
  }
  
  
  ////////////////////// Convert string to float array
  
  public float[] dataToString(String received_string){
    try{
      received_string = received_string.replaceAll(" ", "");
      String[] data_string = received_string.split(",");
      int check = data_string.length;
      println(check);
      if(check == 17){
        float[] data_output = new float[data_string.length];
        for (int i = 0; i < data_string.length; i++){
          data_output[i] = Float.parseFloat(data_string[i]);
        }
        updateFallback(data_output);
        return data_output;
      }
      else{
        return getFallback();
      }
    } catch(ArrayIndexOutOfBoundsException e){
      float[] missed_output = {};
      println("ArrayIndexOutOfBoundsException");
      return missed_output;
    } catch(NullPointerException f) {
      float[] missed_output = {};
     // println("NullPointerException");
      return missed_output;
    } catch(NumberFormatException g) {
      float[] missed_output = {};
      println("NumberFormatException");
      return missed_output;
    }
  }
  
  public void close(){
    socket.close();
  }
}
