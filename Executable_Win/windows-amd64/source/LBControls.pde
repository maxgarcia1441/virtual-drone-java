class LVControls{
  private HashMap<String, HashMap<String, Boolean>> LVKeysPressed;
  private HashMap<String, UDP_server> servers;
  private HashMap<String, PVector> camera_movement;
  private int t1, t2;
  private HashMap<String, Integer[]> times;
  
  private PVector j, center, camera, dir, front, back, left, right, up, down;
  
  
  public LVControls(){
    LVKeysPressed = new HashMap<>();
    camera_movement = new HashMap<>();
    servers = new HashMap<>();
    times = new HashMap<>();
  }
  
  public void addLVControl(UDP_server server){
    PVector emptyV = new PVector(0, 0, 0);
    HashMap<String, Boolean> empty = new HashMap<>();
    LVKeysPressed.put(server.getName(), empty);
    servers.put(server.getName(), server);
    camera_movement.put(server.getName(), emptyV);
    Integer[] ts = new Integer[2];
    times.put(server.getName(), ts);
  }
  
  public void LVotherKeys(String name){
    times.get(name)[0] = times.get(name)[1];
    times.get(name)[1] = int(millis() / 40);
    if(times.get(name)[1] != times.get(name)[0]){
      String[] keys = servers.get(name).getKeysPressed();
      if(keys != null){
        for(String s : LVKeysPressed.get(name).keySet()) LVKeysPressed.get(name).replace(s, false);
        for(String keyy : keys){
          LVKeysPressed.get(name).put(keyy, true);
        }
      }
    }
  }
  
  public boolean LVisPressed(String name, String keyy){
    if(LVKeysPressed.get(name).containsKey(keyy)){
      return LVKeysPressed.get(name).get(keyy);
    }else{
      return false;
    }
  }
  
  public PVector moveUseCam(String name, Camera cam){
    camera_movement.get(name).mult(0);
    
    j = null;
    j = new PVector(0, -1, 0);
    center = cam.getObj_position();
    camera = cam.getCam_position();
    dir = PVector.sub(center, camera);
    dir.y = 0;
    dir.normalize();
    front = dir;
    back = PVector.mult(front, -1);
    left = dir.cross(j).normalize();
    right = PVector.mult(left, -1);
    up = j;
    down = PVector.mult(j, -1);
    
    if (LVisPressed(name, "W")) camera_movement.get(name).add(front);
    if (LVisPressed(name, "S")) camera_movement.get(name).add(back);
    if (LVisPressed(name, "D")) camera_movement.get(name).add(right);
    if (LVisPressed(name, "A")) camera_movement.get(name).add(left);
    if (LVisPressed(name, "UP")) camera_movement.get(name).add(up);
    if (LVisPressed(name, "DOWN")) camera_movement.get(name).add(down);
    
    camera_movement.get(name).mult(10);
    //println(camera_movement);
    return camera_movement.get(name);
  }
}
