import java.util.HashMap;

class Solids{
  private ArrayList<Solid> solids;
  private HashMap<String, Solid> map;
  private HashMap<Integer, String> index_to_name;
  private int index;
  private float gravity;
  
  ArrayList<Character> one = new ArrayList<Character>();
  ArrayList<Character> two = new ArrayList<Character>();
  
  int times = 0;
  int i = 0;
  
  private ArrayList<ArrayList<Solid>> input;
  private ArrayList<Solid[]> temp_res;
  private ArrayList<Solid[]> permutations;
  private Solid[] current;
  
  private Solid solid_1;
  private Solid solid_2;
  
  private float distance[];
  private float abs_distance[];
  private float dimensions[];
  
  public Solids(){
    solids = new ArrayList<Solid>();
    map = new HashMap<>();
    index_to_name = new HashMap<>();
    index = 0;
    input = new ArrayList<ArrayList<Solid>>();
    temp_res = new ArrayList<Solid[]>();
    permutations = new ArrayList<Solid[]>();
    current = new Solid[2]; 
  }
  
  public void add(Solid body){
    map.put(body.getName(), body);
    index_to_name.put(index, body.getName());
    solids.add(body);
    index += 1;
    
    input.removeAll(input);
    temp_res.removeAll(temp_res);
    permutations.removeAll(permutations);
    input.add(solids);
    input.add(solids);
    current[0] = null;
    current[1] = null;
    generatePermutations(input, temp_res, 0, current);
    permutations = allPermutations(temp_res);
  }
  
  public void updateState(){
    for(int a=0; a<solids.size(); a++){                         // This updates the value from the map to the array
      map.get(index_to_name.get(a)).update();         // This loop goes through all solids
      solids.set(a, get(index_to_name.get(a)));
    }
  }
  
  public Solid get(String name) {
    return map.get(name);                // This only changes the map, updating the Array is done below
  }

  public ArrayList<Solid[]> allPermutations(ArrayList<Solid[]> in){
    ArrayList<Solid[]> result = new ArrayList<Solid[]>();
    for(int i=0; i<in.size(); i++){
      Solid[] combination = new Solid[in.get(0).length];
      for(int e=0; e<in.get(0).length; e++){
        combination[in.get(0).length - 1 - e] = in.get(i)[e];   //inverting it
      }
      boolean same = false;
      for(int e=0; e<in.get(0).length; e++){
        Solid[] checker = new Solid[in.get(0).length - 1];
        for(int r=0; r<in.get(0).length - 1; r++){
          boolean passed_value = false;
          if(r == e) passed_value = true;
          if(passed_value){
            checker[r] = combination[r + 1];
          }else{
            checker[r] = combination[r];
          }
          if(combination[e] ==  checker[r]) same = true;
        }
      }
      for(int r=0; r<result.size(); r++){
        boolean first = result.get(r)[0].getName() == in.get(i)[0].getName() && result.get(r)[1].getName() == in.get(i)[1].getName();
        boolean second = result.get(r)[0].getName() == combination[0].getName() && result.get(r)[1].getName() == combination[1].getName();
        if(first || second) same = true;
      }
      if(!same){
        result.add(in.get(i));
        result.add(combination);
      }
    }
    return result;
  }
  
  private void generatePermutations(ArrayList<ArrayList<Solid>> lists, ArrayList<Solid[]> result, int depth, Solid[] current) {
    if (depth == lists.size()) {
        result.add(current);
        return;
    }

    for (int i = 0; i < lists.get(depth).size(); i++) {
      current[depth] = lists.get(depth).get(i);
     // println("depth: ", depth, current[depth].getName());
      generatePermutations(lists, result, depth + 1, current.clone());
    }
  }
  
  public void containSolids(Sphere sphere){
    for(Solid s : solids){
      if(PVector.sub(s.getPosition(), sphere.getPosition()).mag() > sphere.getRadius()){
        s.setVelocity(PVector.mult(s.getVelocity(), -1));
        PVector newP = PVector.sub(s.getPosition(), sphere.getPosition());
        newP.normalize();
        newP.mult(sphere.getRadius());
        s.setPosition(newP);
      }
    }
  }
  
  public void checkCollisions() {
    updateState();
    for(int i=0; i<permutations.size(); i++){
      solid_1 = permutations.get(i)[0];
      solid_2 = permutations.get(i)[1];
      
      float distance[] = new float[3];
      float abs_distance[] = new float[3];
      float dimensions[] = new float[3];
      
      dimensions[0] = (solid_1.getWidth() + solid_2.getWidth())/2;
      dimensions[1] = (solid_1.getHeight() + solid_2.getHeight())/2;
      dimensions[2] = (solid_1.getDepth() + solid_2.getDepth())/2;
      
      distance[0] = (solid_2.position_c.x - solid_1.position_c.x) - dimensions[0];
      distance[1] = (solid_2.position_c.y - solid_1.position_c.y) - dimensions[1];
      distance[2] = (solid_2.position_c.z - solid_1.position_c.z) - dimensions[2];
      
      abs_distance[0] = abs(solid_2.position_c.x - solid_1.position_c.x) - dimensions[0];
      abs_distance[1] = abs(solid_2.position_c.y - solid_1.position_c.y) - dimensions[1];
      abs_distance[2] = abs(solid_2.position_c.z - solid_1.position_c.z) - dimensions[2];
      
      float d = abs(abs_distance[0]);
      int axis = 0;
      for(int e=0; e<3; e++){
        if(abs(abs_distance[e]) < d){
          d = abs(abs_distance[e]);
          axis = e;
        }
      }
      
    /*  if(abs_distance[0] + solid_1.velocity.y + solid_2.velocity.y >= 0){
        PVector f = new PVector(0, solid_1.mass * 0.1, 0);
        solid_1.position.add(f);
        f.y = solid_2.mass * 0.1;
        solid_2.position.add(f);
      }*/
      
      if(abs_distance[0] < 0 && abs_distance[1] < 0 && abs_distance[2] < 0){
        //println(solid_1.getName(), " and", solid_2.getName());
        switch(axis){
          case 0:
            if(solid_2.getX() > solid_1.getX()){
              if(solid_1.movable()) solid_1.moveTo(solid_2.position_c.x - dimensions[0], solid_1.position_c.y, solid_1.position_c.z);
            }else{
              if(solid_1.movable()) solid_1.moveTo(solid_2.position_c.x + dimensions[0], solid_1.position_c.y, solid_1.position_c.z);
            }
            break;
          case 1:
            if(solid_2.getY() > solid_1.getY()){
              if(solid_1.movable()) solid_1.moveTo(solid_1.position_c.x, solid_2.position_c.y - dimensions[1], solid_1.position_c.z);
            }else{
              if(solid_1.movable()) solid_1.moveTo(solid_1.position_c.x, solid_2.position_c.y + dimensions[1], solid_1.position_c.z);
            }
            break;
          case 2:
            if(solid_2.getZ() > solid_1.getZ()){
              if(solid_1.movable()) solid_1.moveTo(solid_1.position_c.x, solid_1.position_c.y, solid_2.position_c.z - dimensions[2]);
            }else{
              if(solid_1.movable()) solid_1.moveTo(solid_1.position_c.x, solid_1.position_c.y, solid_2.position_c.z + dimensions[2]);
            }
            break;
        }
        solid_1.collision(solid_2);
      }
    }
  }
  
  public void gravity(float gravity){
    PVector weight = new PVector(0, gravity, 0);
    solids.forEach(s -> s.setWeight(weight));
  }
  
  public void resistance(){
    for(int i=0; i<solids.size(); i++){
      if(solids.get(i).movable()) solids.get(i).push(PVector.mult(solids.get(i).forces, -0.05));
    }
  }
  
  public float mean(ArrayList<Float[]> array){
    float mean = 0;
    for(int e=0; e<array.size(); e++){
      mean += array.get(e)[1] / array.size();
    }
    return mean;
  }
  
  public String index_to_name(int index){
    return index_to_name.get(index);
  }
  
  public Integer nSolids(){
    return solids.size();
  }
  
  public ArrayList<Solid> getSolids(){
    return solids;
  }
  
}
