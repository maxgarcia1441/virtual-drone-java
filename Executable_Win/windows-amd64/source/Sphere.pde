class Sphere{
  private String filename;
  private PShape shape;
  private PVector coordinates;
  private PImage image;
  private int radius;
  private float[] angles;
  private boolean loaded;
  private boolean loading;
  
  public Sphere(String filename, float x, float y, float z){
    this.filename = filename;
    angles = new float[2];
    radius = 900;
    coordinates = new PVector(x, y, z);
    loaded = false;
    loading = false;
    load();
  }
  
  public void rotate(float a_XZ, float a_XZ_Y){
    angles[0] = a_XZ;
    angles[1] = a_XZ_Y;
  }
  
  public void display(){
    if(loaded){
    pushMatrix();
    shape.setStroke(0);
    translate(coordinates.x, coordinates.y, coordinates.z);
    rotateY(radians(angles[0]));
    rotateZ(radians(angles[1]));
    shape(shape);
    popMatrix();
    }
  }
  
  public void load(){
    loading = true;
    sphereDetail(20);
    image = loadImage(filename);
    shape = createShape(SPHERE, radius);
    shape.setTexture(image);
    shape.setStroke(0);
    image = null;
    System.gc();
    loaded = true;
    loading = false;
  }
  
  public void unLoad(){
    image = null;
    shape = null;
    loaded = false;
  }
  
  public PVector getPosition(){
    return coordinates;
  }
  
  public boolean isLoaded(){
    return loaded;
  }
  
  public boolean isLoading(){
    return loading;
  }
  
  public String filename(){
    return filename;
  }
  
  public int getRadius(){
    return radius;
  }
  
}



/*
Correct (rotate) spheres

   Make tilting drone
   
   Make player actually look like a drone
   
Make a function to add Spheres to Solids

Add walls (preferably non-cubic)

Add a more realistic floor

Improve brighntess

Commnets about university*/
